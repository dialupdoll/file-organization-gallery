package io.gitlab.dahlterm.Caching;

import io.gitlab.dahlterm.Logging;

import java.util.ArrayList;
import java.util.HashMap;

public class UsageBasedTimeInvalidatedCache<K, E> {
    private class CacheEntry {
        E wrapped_entry;
        int secondsToKeep;
        int defaultSeconds;

        public CacheEntry(E init_entry, int init_secondsToKeep) {
            wrapped_entry = init_entry;
            secondsToKeep = init_secondsToKeep;
            defaultSeconds = init_secondsToKeep;
        }

        private void resetTimer() {
            secondsToKeep = defaultSeconds;
        }

        private boolean updateTimer(int delayedSeconds) {
            if (secondsToKeep > delayedSeconds) {
                secondsToKeep -= delayedSeconds;
                return true;    /// still time left to keep.
            } else {
                return false;
            }
        }
    }

    private HashMap<K, CacheEntry> contents = new HashMap<>();


    public UsageBasedTimeInvalidatedCache() {
        this(5);
    }

    private Integer delay = 1;

    private class UpdateThread implements Runnable {
        @Override
        public void run() {
            while (true) {
                synchronized (delay) {
                    /// WAIT DELAY
                    try {
                        Thread.sleep(delay * 1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    /// UPDATE ALL THE ELEMENTS
                    synchronized (contents) {
                        ArrayList<CacheEntry> toRemove = new ArrayList<>();
                        for (CacheEntry iterator :
                                contents.values()) {
                            boolean shouldRemove =
                                    !iterator.updateTimer(delay);
                            if (shouldRemove) {
                                toRemove.add(iterator);
                            }
                        }
                        for (CacheEntry iterator : toRemove) {
                            contents.remove(iterator);
                        }
                        if (!toRemove.isEmpty()) {
                            Logging.log(Logging.FLOW,
                                    "Cache Removal ran: " + toRemove.size() + " " +
                                            "removed.");
                            Logging.log(Logging.FLOW,
                                    +contents.size() + " remain cached.");
                        }
                    }
                }
            }
        }
    }

    public UsageBasedTimeInvalidatedCache(int secondsDelay) {
        setDelay(secondsDelay); /// also starts the thread.
    }

    private void setDelay(int secondsDelay) {
        if (cacheUpdater != null) {
            endThread();
        }
        synchronized (delay) {
            delay = secondsDelay;
        }
        cacheUpdater = new Thread(new UpdateThread());
        cacheUpdater.start();
    }


    public void add(K id, E entry, int secondsToKeep) {
        synchronized (contents) {
            if (!checkIfCached(id)) {
                contents.put(id, new CacheEntry(entry, secondsToKeep));
            } else {
                Logging.log(Logging.ERROR,
                        id.toString() + " is already present. " + secondsToKeep + " time remaining in cache.");
            }
        }
    }

    public boolean checkIfCached(K id) {
        synchronized (contents) {
            return contents.containsKey(id);
        }
    }

    public E fetchOrNull(K id) {
        synchronized (contents) {
            if (checkIfCached(id)) {
                CacheEntry focus = contents.get(id);
                focus.resetTimer();
                return focus.wrapped_entry;
            }
            return null;
        }
    }

    private Thread cacheUpdater = null;

    public void endThread() {
        if (cacheUpdater != null) {
            cacheUpdater.interrupt();
            cacheUpdater = null;
        }
    }
}
