package io.gitlab.dahlterm.FileOrganizationGallery.ui;

import io.gitlab.dahlterm.Async.Task;
import io.gitlab.dahlterm.Async.TaskQueue;
import io.gitlab.dahlterm.FileOrganizationGallery.EntryPoint;
import io.gitlab.dahlterm.FileOrganizationGallery.Toolbox;
import io.gitlab.dahlterm.FileOrganizationGallery.galleryabstractions.Entry;
import io.gitlab.dahlterm.FileOrganizationGallery.galleryabstractions.FileDisplay.EntryPropertyEditor;
import io.gitlab.dahlterm.FileOrganizationGallery.galleryabstractions.FileDisplay.Extensions.FileDisplayFallback;
import io.gitlab.dahlterm.FileOrganizationGallery.galleryabstractions.FileDisplay.Extensions.FileDisplayImageFile;
import io.gitlab.dahlterm.FileOrganizationGallery.galleryabstractions.FileDisplay.FileDisplayImplementation;
import io.gitlab.dahlterm.FileOrganizationGallery.galleryabstractions.GalleryData;
import io.gitlab.dahlterm.FileOrganizationGallery.galleryabstractions.tagging.TagView;
import io.gitlab.dahlterm.FileOrganizationGallery.other.SimpleCodeSnippet;
import io.gitlab.dahlterm.Logging;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.filechooser.FileFilter;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Optional;
import java.util.Random;

public class GalleryBrowserWindow implements SimpleCodeSnippet {

    final int fullViewMode = 0, thumbMode = 1, neither = 2;
    final String CARD_FULL_VIEW = "fullview";
    final String CARD_THUMBNAIL_VIEW = "thumbnailview";
    final String CARD_EDIT_VIEW = "editview";
    public ArrayList<Entry> view = null;
    JFrame window = null;
    GalleryData galleryData = null;
    JPanel previewPanel = null;
    JPanel metadataEditor = null;
    JPanel thumbPanel = null;
    int mode = 0;
    FileDisplayImplementation supportedFileDisplayHandlers[] = {
            new FileDisplayImageFile()
    };
    FileDisplayImplementation fallback = new FileDisplayFallback();
    JButton fullViewButton;
    JButton thumbnailViewButton;
    JButton settingsViewButton;
    JPanel informationPanel;
    TagView tagAreaFullView = null;
    TagView tagAreaThumbnailView = null;
    CardLayout modes;
    JPanel modeZone;
    private int entrySelection = 0;
    static File lastBrowsedDirectoryToSelectAdditions = null;

    StatusPanel statusBar = null;
    TaskQueue tasks = new TaskQueue();  /// BACKGROUND TASK QUEUE

    static BufferedImage thumbIcon, fullIcon, gearIcon;
    Color recolored = Color.orange;

    private BufferedImage loadImage(String s) throws IOException {
        URL url = getClass().getResource("/" + s);

        Image result =
                ImageIO.read(url.openStream());
        BufferedImage seeThrough = Toolbox.toBufferedImage(result);
        for (int x = 0; x < seeThrough.getWidth(); x++) {
            for (int y = 0; y < seeThrough.getHeight(); y++) {
                int pixInt = seeThrough.getRGB(x, y);

                Color replaceColor = Color.black;
                if (pixInt == replaceColor.getRGB()) {
                    //Calculation will be done here
                    seeThrough.setRGB(x, y, recolored.getRGB());     //transparent
                }
            }
        }
        return seeThrough;
    }

    public void loadIcons() {
        new Thread(() -> {
            try {
                thumbIcon = loadImage("thumbicon.png");
                if (thumbIcon != null) {
                    thumbnailViewButton.setIcon(new ImageIcon(thumbIcon));
                    thumbnailViewButton.setText("");
                }
                fullIcon = loadImage("fullicon.png");
                if (fullIcon != null) {
                    fullViewButton.setIcon(new ImageIcon(fullIcon));
                    fullViewButton.setText("");
                }
                gearIcon = loadImage("gearicon.png");
                if (gearIcon != null) {
                    settingsViewButton.setIcon(new ImageIcon(gearIcon));
                    settingsViewButton.setText("");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }).start();
    }

    public GalleryBrowserWindow(GalleryData backend) {
        if (backend == null) {
            System.err.println("A");
        }
        galleryData = backend;
        window = new JFrame();
        window.setTitle(backend.galleryNameGet() + " - File Organization " +
                "Gallery");
        window.setMinimumSize(new Dimension(765, 765));
        window.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        window.setLayout(new BorderLayout());


        modes = new CardLayout();
        modeZone = new JPanel();
        {
            modeZone.setLayout(modes);

            JPanel fullView = new JPanel();
            {
                fullView.setLayout(new BorderLayout());

                JPanel rightHalf = new JPanel();
                {
                    rightHalf.setLayout(new BorderLayout());
                    JPanel navbar = new JPanel();
                    {
                        navbar.setLayout(new GridLayout(0, 3));

                        JButton previous = new JButton("<");
                        {
                            navbar.add(previous);
                            previous.addActionListener(new ActionListener() {
                                @Override
                                public void actionPerformed(ActionEvent actionEvent) {
                                    if (view.isEmpty())
                                        return;
                                    entrySelection--;
                                    updateFullViewPanel();

                                    onSelect();
                                }
                            });
                        }

                        JButton random = new JButton("random");
                        {
                            navbar.add(random);
                            random.addActionListener(new ActionListener() {
                                @Override
                                public void actionPerformed(ActionEvent actionEvent) {
                                    if (view.isEmpty())
                                        return;
                                    if (view.size() < 4) {
                                        entrySelection++;
                                        updateFullViewPanel();
                                        onSelect();
                                        return;
                                    }

                                    Random r = new Random();
                                    r.setSeed(System.currentTimeMillis());
                                    int lastSelection = entrySelection;
                                    while (entrySelection == lastSelection) {
                                        entrySelection = r.nextInt(view.size());
                                    }
                                    updateFullViewPanel();

                                    onSelect();
                                }
                            });
                        }

                        JButton next = new JButton(">");
                        {
                            navbar.add(next);
                            next.addActionListener(new ActionListener() {
                                @Override
                                public void actionPerformed(ActionEvent actionEvent) {
                                    if (view.isEmpty())
                                        return;
                                    entrySelection++;

                                    updateFullViewPanel();

                                    onSelect();
                                }
                            });
                        }

                        rightHalf.add(navbar, BorderLayout.NORTH);
                    }

                    tabmenu = new JTabbedPane(JTabbedPane.BOTTOM);
                    {
                        previewPanel = new JPanel();
                        {
                            previewPanel.setLayout(new BorderLayout());
                            previewPanel.setBorder(
                                    BorderFactory.createEmptyBorder(
                                            10, 10,
                                            10, 10));
                        }
                        tabmenu.addTab("Viewing", previewPanel);

                        metadataEditor = new JPanel();
                        {
                            metadataEditor.setLayout(new BorderLayout());
                            metadataEditor.setBorder(
                                    BorderFactory.createEmptyBorder(
                                            10, 10,
                                            10, 10));
                        }
                        tabmenu.addTab("Metadata", metadataEditor);

                        JPanel propertyEditor = new JPanel();
                        {
                            propertyEditor.setLayout(new BorderLayout());
                            propertyEditor.add(
                                    new EntryPropertyEditor(
                                            null),
                                    BorderLayout.CENTER);
                        }
                        tabmenu.addTab("Properties", propertyEditor);
                    }
                    rightHalf.add(tabmenu, BorderLayout.CENTER);

                }

                tagAreaFullView = new TagView(true, this, galleryData);
                JSplitPane horizontalSplitter = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, tagAreaFullView, rightHalf);
                fullView.add(horizontalSplitter, BorderLayout.CENTER);
                modeZone.add(fullView, CARD_FULL_VIEW);
            }

            /// TODO: THUMBNAIL VIEW DOES NOT HANDLE PAGINATION YET
            JPanel thumbnailView = new JPanel();
            {
                JScrollPane thumbnailScroller = new JScrollPane();
                {
                    thumbPanel = new VerticalScrollOnlyPanel();
                    thumbPanel.setLayout(new WrapLayout());
                    tagAreaThumbnailView = new TagView(false, this, galleryData);

                    thumbnailScroller.setViewportView(thumbPanel);
                }
                thumbnailView.setLayout(new BorderLayout());
                thumbnailView.add(tagAreaThumbnailView, BorderLayout.WEST);
                thumbnailView.add(thumbnailScroller, BorderLayout.CENTER);
                modeZone.add(thumbnailView, CARD_EDIT_VIEW);
                modes.addLayoutComponent(thumbnailView, CARD_THUMBNAIL_VIEW);
            }

            JPanel gallerySettingsView = new JPanel();
            {
                modeZone.add(gallerySettingsView, CARD_EDIT_VIEW);
                modes.addLayoutComponent(gallerySettingsView, CARD_EDIT_VIEW);
                gallerySettingsView.setLayout(new BorderLayout());
                class settingDescription {
                    String description;

                    public settingDescription(String descrip) {
                        description = descrip;
                    }

                    public void moreInformation() {
                        JOptionPane.showMessageDialog(gallerySettingsView, description);
                    }
                }
                class utilitySettingPanelCreator {
                    public static JPanel newSetting(String name, Component setting,
                                                    settingDescription descrip) {
                        JPanel result = new JPanel();
                        {
                            result.setLayout(new BorderLayout());
                            JLabel identifier = new JLabel(name);
                            identifier.setBorder(BorderFactory.createEmptyBorder(
                                    0, 0, 0, 15));
                            identifier.setMaximumSize(setting.getPreferredSize());
                            result.add(identifier, BorderLayout.LINE_START);
                            setting.setMaximumSize(setting.getPreferredSize());

                            JPanel settingPanel = new JPanel();
                            settingPanel.setLayout(new BorderLayout());
                            settingPanel.add(setting, BorderLayout.CENTER);
                            settingPanel.setBorder(BorderFactory.createEmptyBorder(
                                    5, 0, 5, 0));
                            result.add(settingPanel, BorderLayout.CENTER);

                            JPanel moreInformation = new JPanel();
                            JButton moreInformationButton = new JButton("?");
                            moreInformation.add(moreInformationButton);
                            moreInformation.setBorder(BorderFactory.createEmptyBorder(
                                    0, 0, 0, 15));
                            moreInformation.setMaximumSize(
                                    moreInformation.getPreferredSize());
                            moreInformationButton.addActionListener(new ActionListener() {
                                @Override
                                public void actionPerformed(ActionEvent actionEvent) {
                                    descrip.moreInformation();
                                }
                            });
                            result.add(moreInformation, BorderLayout.LINE_END);
                        }
                        return result;
                    }
                }
                JPanel settingsList = new JPanel();
                {
                    settingsList.setLayout(new BoxLayout(settingsList, BoxLayout.Y_AXIS));
                    int i = 20;
                    settingsList.setBorder(BorderFactory.createEmptyBorder(
                            i, i, i, 0));
                    JPanel galleryName = new JPanel();
                    {
                        galleryName.setLayout(new BorderLayout());
                        JTextField galleryNameEdit = new JTextField();
                        galleryName.add(galleryNameEdit, BorderLayout.CENTER);
                        galleryNameEdit.addFocusListener(new FocusListener() {
                            @Override
                            public void focusGained(FocusEvent focusEvent) {
                                galleryNameEdit.setText(galleryData.galleryNameGet());
                            }

                            @Override
                            public void focusLost(FocusEvent focusEvent) {
                                galleryData.galleryNameSet(galleryNameEdit.getText());
                                window.setTitle(backend.galleryNameGet() + " " +
                                        "- File Organization Gallery");
                            }
                        });
                        galleryNameEdit.setText(galleryData.galleryNameGet());
                    }
                    settingsList.add(utilitySettingPanelCreator.newSetting(
                            "Gallery Title", galleryName,
                            new settingDescription(
                                    "the name of the gallery, and " +
                                            "thus the name that will " +
                                            "show up atop the window.")));
                    JButton addFile = new JButton("Browse");

                    addFile.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent actionEvent) {
                            final JFileChooser javaFileChooser = new JFileChooser();
                            for (FileDisplayImplementation impl :
                                    supportedFileDisplayHandlers) {
                                FileFilter potentialFilter =
                                        impl.supportedFilesFilter();
                                if (potentialFilter != null) {
                                    javaFileChooser.addChoosableFileFilter(potentialFilter);
                                }
                            }
                            javaFileChooser.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
                            javaFileChooser.setMultiSelectionEnabled(true);
                            if (lastBrowsedDirectoryToSelectAdditions == null) {
                                lastBrowsedDirectoryToSelectAdditions =
                                        new File(System.getProperty("user.dir"));
                            }
                            javaFileChooser.setCurrentDirectory(lastBrowsedDirectoryToSelectAdditions);
                            int returnVal = javaFileChooser.showOpenDialog(window);
                            if (returnVal == JFileChooser.APPROVE_OPTION) {
                                lastBrowsedDirectoryToSelectAdditions =
                                        javaFileChooser.getCurrentDirectory();

                                File[] files = javaFileChooser.getSelectedFiles();

                                for (File file : files) {
                                    //e = galleryData.addEntry(file);
                                    ImportFileTask current = new ImportFileTask();
                                    current.target = file;
                                    tasks.add(current);
                                    tasks.startAsyncIfNotWorking();
                                }

                            }
                        }
                    });
                    settingsList.add(utilitySettingPanelCreator.
                            newSetting("Add File To " +
                                            "Gallery", addFile,
                                    new settingDescription(
                                            "Opens a file browser to select what you " +
                                                    "want to add to the gallery")));
                }
                gallerySettingsView.add(settingsList, BorderLayout.NORTH);
            }
            loadIcons();
        }

        window.add(modeZone, BorderLayout.CENTER);

        statusBar = new StatusPanel();
        window.add(statusBar.displayComponent(), BorderLayout.SOUTH);
        statusBar.setTaskQueue(tasks);

        JPanel switcher = new JPanel();
        {
            switcher.setLayout(new BorderLayout());
            JPanel topAlignment = new JPanel();
            topAlignment.setLayout(new BoxLayout(topAlignment,
                    BoxLayout.Y_AXIS));
            switcher.add(topAlignment, BorderLayout.SOUTH);

            fullViewButton = new JButton("F");
            thumbnailViewButton = new JButton("T");
            settingsViewButton = new JButton("S");

            topAlignment.add(settingsViewButton);
            settingsViewButton.setToolTipText("Gallery Settings");
            settingsViewButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent actionEvent) {
                    fullViewButton.setEnabled(true);
                    thumbnailViewButton.setEnabled(true);
                    settingsViewButton.setEnabled(false);
                    modes.show(modeZone, CARD_EDIT_VIEW);
                    mode = neither;
                }
            });

            final TagView fullVTags = tagAreaFullView;

            final TagView fullTTags = tagAreaThumbnailView;

            thumbnailViewButton.setToolTipText("Thumbnail View");
            topAlignment.add(thumbnailViewButton);

            thumbnailViewButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent actionEvent) {
                    switchToThumbView();
                }
            });

            fullViewButton.setEnabled(false);
            fullViewButton.setToolTipText("Full View");
            topAlignment.add(fullViewButton);
            tagAreaFullView.search(backend);
            tagAreaThumbnailView.search(backend);

            fullViewButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent actionEvent) {

                    switchToFullView();
                }
            });

            window.add(switcher, BorderLayout.EAST);
        }

        bottomHalfOfMetaEditor = new JPanel();
        {
            bottomHalfOfMetaEditor.setLayout(new BorderLayout());
            JButton remove = new JButton("Remove from Gallery");
            remove.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent actionEvent) {
                    if (JOptionPane.showConfirmDialog(remove, "Remove " +
                                    "file " +
                                    "from gallery?",
                            "Confirm removal", JOptionPane.YES_NO_OPTION,
                            JOptionPane.WARNING_MESSAGE) != 0) {
                        return;
                    }
                    Entry e = view.get(entrySelection);
                    e.removeEntry();

                    switchToThumbView();
                    update();

                    onSelect();
                }
            });
            bottomHalfOfMetaEditor.add(remove, BorderLayout.EAST);
        }
        recolored = thumbnailViewButton.getForeground();

        loadIcons();
        update();
        updateThumbPanel();
        updateFullViewPanel();
        window.pack();
        window.setVisible(true);
        EntryPoint.windowCount++;
        window.addWindowListener(new WindowListener() {
            @Override
            public void windowOpened(WindowEvent e) {

            }

            @Override
            public void windowClosing(WindowEvent e) {
                EntryPoint.checkShouldClose();
            }

            @Override
            public void windowClosed(WindowEvent e) {

            }

            @Override
            public void windowIconified(WindowEvent e) {

            }

            @Override
            public void windowDeiconified(WindowEvent e) {

            }

            @Override
            public void windowActivated(WindowEvent e) {

            }

            @Override
            public void windowDeactivated(WindowEvent e) {

            }
        });
    }

    public class ImportFileTask extends Task.SimpleTask {
        File target;

        @Override
        public TaskStatus completeTask() {
            synchronized (galleryData) {
                Entry e = galleryData.addEntry(target);
                if (e != null) {
                    if (mode == thumbMode) {
                        Toolbox.swingThreadSafeMessage(GalleryBrowserWindow.this::updateThumbPanel);
                    }
                    Logging.log(Logging.FLOW,
                            "Successfully loaded " +
                                    "entry into data");
                    return TaskStatus.SUCCESSFUL;
                } else {
                    Logging.log(Logging.FLOW,
                            "Unsuccessfully loaded " +
                                    "entry into data");
                    return TaskStatus.FAILED_NO_RETRY;
                }
            }
        }

        @Override
        public boolean equalTo(Task t) {
            return false;
        }
    }

    JTabbedPane tabmenu;

    public void switchToThumbView() {
        fullViewButton.setEnabled(true);
        thumbnailViewButton.setEnabled(false);
        settingsViewButton.setEnabled(true);

        tagAreaThumbnailView.setSearch(tagAreaFullView.getSearch(),
                tagAreaFullView.currentSearchSettings());

        modes.show(modeZone, CARD_THUMBNAIL_VIEW);
        Toolbox.awtRepaint(modeZone);

        mode = thumbMode;
        update();
    }

    public void switchToFullView() {
        tabmenu.setSelectedIndex(0);
        fullViewButton.setEnabled(false);
        thumbnailViewButton.setEnabled(true);
        settingsViewButton.setEnabled(true);

        tagAreaFullView.setSearch(tagAreaThumbnailView.getSearch(),
                tagAreaThumbnailView.currentSearchSettings());

        modes.show(modeZone, CARD_FULL_VIEW);
        Toolbox.awtRepaint(modeZone);
        mode = fullViewMode;
        updateFullViewPanel();
    }

    public FileDisplayImplementation determineImplementation(File target) {
        String extension;
        Optional<String> extensionResult =
                Toolbox.getExtensionByStringHandling(
                        target.getAbsolutePath());
        if (!extensionResult.isPresent()) {
            System.err.println("Couldn't get extension");
            extension = target.getName();
        } else {
            extension = extensionResult.get();
        }

        for (FileDisplayImplementation impl : supportedFileDisplayHandlers) {
            if (impl.canDisplay(extension)) {
                return impl;
            }
        }
        return fallback;
    }

    JPanel bottomHalfOfMetaEditor;

    public void updateFullViewPanel() {
        validateSelection();

        if (view.isEmpty()) {
            previewPanel.removeAll();
            previewPanel.add(new JLabel("This gallery is empty! Open the Settings menu [S] " +
                    "to add files!"));
            metadataEditor.add(new JLabel("This gallery is empty! Open the Settings menu [S] " +
                    "to add files!"));

            Toolbox.awtRepaint(previewPanel);
        } else {
            Entry e = view.get(entrySelection);
            FileDisplayImplementation impl = determineImplementation(e.wrappedEntry());
            JComponent previewcomp = impl.displayFileFull(e);
            JComponent metacomp = impl.fileDisplayExtended(e);
            metadataEditor.removeAll();
            metadataEditor.add(metacomp, BorderLayout.NORTH);
            metadataEditor.add(bottomHalfOfMetaEditor, BorderLayout.SOUTH);
            previewPanel.removeAll();
            previewPanel.add(previewcomp, BorderLayout.CENTER);
            Toolbox.awtRepaint(metadataEditor);
            tagAreaFullView.setCurrentlySelectedEntry(e);
            tagAreaFullView.updateEntryTags();
            Toolbox.awtRepaint(previewPanel);
        }
    }

    void validateSelection() {
        if (view == null) {
            entrySelection = 0;
            return;
        }
        if (view.isEmpty()) {
            entrySelection = 0;
            return;
        }

        if (entrySelection > view.size() - 1) {
            entrySelection = 0;
        }

        if (entrySelection < 0) {
            entrySelection = view.size() - 1;
        }
    }

    public void onSelect() {
        validateSelection();
        tagAreaFullView.setCurrentlySelectedEntry(view.get(entrySelection));
        tagAreaFullView.updateEntryTags();
    }

    public void updateThumbPanel() {

        thumbPanel.removeAll();
        if (view == null) {
            return;
        }
        for (Entry e : view) {
            FileDisplayImplementation impl = determineImplementation(e.wrappedEntry());
            thumbPanel.add(impl.displayFileThumbnail(e,
                    new SimpleCodeSnippet() {
                        @Override
                        public void update() {
                            entrySelection = view.indexOf(e);
                            onSelect();
                            switchToFullView();
                        }
                    }));
        }
        Toolbox.awtRepaint(thumbPanel);
    }

    TagView search = null;

    @Override
    public void update() {
        view = new ArrayList<Entry>();
        if (galleryData.entries() != null) {
            if (mode == fullViewMode) {
                search = tagAreaFullView;
            }
            if (mode == thumbMode) {
                search = tagAreaThumbnailView;
            }
            view.clear();
            ArrayList<Entry> result = search.search(galleryData);
            if (result != null) {
                if (!result.isEmpty()) {
                    view.addAll(result);
                }
            }
            if (view.isEmpty()) {
                if (search.getSearch().isBlank()) {
                    view.addAll(galleryData.entries());
                    Collections.shuffle(view);
                }
            }
        }


        if (mode == fullViewMode) {
            switchToThumbView();
            //updateFullViewPanel();
        }
        if (mode == thumbMode) {
            updateThumbPanel();
        }
        validateSelection();
    }

}
