package io.gitlab.dahlterm.FileOrganizationGallery.ui;

import com.grack.nanojson.JsonAppendableWriter;
import com.grack.nanojson.JsonArray;
import com.grack.nanojson.JsonParser;
import com.grack.nanojson.JsonWriter;
import io.gitlab.dahlterm.FileOrganizationGallery.EntryPoint;
import io.gitlab.dahlterm.FileOrganizationGallery.galleryabstractions.GalleryData;
import io.gitlab.dahlterm.FileOrganizationGallery.galleryabstractions.GalleryDataProvider;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.*;
import java.util.ArrayList;

public class GalleryListWindow {

    public static final int MAX_HISTORY = 5;
    JFrame window = null;

    JPanel historyList = null;
    JCheckBox shouldClose = null;

    final String aboutText =
            "This software contains unmodified binary redistributions for" +
                    System.lineSeparator() +
                    "H2 database engine (https://h2database.com/)," +
                    System.lineSeparator() +
                    "which is dual licensed and available under the MPL 2.0" +
                    System.lineSeparator() +
                    "(Mozilla Public License) or under the EPL 1.0 (Eclipse " +
                    System.lineSeparator() +
                    "Public License).  An original copy of the license " +
                    System.lineSeparator() +
                    "agreement can be found at: " +
                    System.lineSeparator() +
                    "https://h2database.com/html/license.html, as well " +
                    System.lineSeparator() +
                    "as unmodified binary redistributions for nanojson" +
                    System.lineSeparator() +
                    "(https://github.com/mmastrac/nanojson) which is" +
                    System.lineSeparator() +
                    "dual-licensed under the MIT and Apache Public License. " +
                    System.lineSeparator() +
                    "All other code is written by HoodieAK.";

    public GalleryListWindow() {
        window = new JFrame();
        window.setTitle("Gallery Selection");

        JPanel toolbar = new JPanel();
        toolbar.setLayout(new BoxLayout(toolbar, BoxLayout.X_AXIS));

        JButton loadGalleryButton = new JButton("Load Gallery");
        final JFileChooser fc = new JFileChooser();

        loadGalleryButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
                int returnVal = fc.showOpenDialog(window);
                if (returnVal == JFileChooser.APPROVE_OPTION) {
                    GalleryData gallery = new GalleryDataProvider().providerGet().openGallery(fc.getSelectedFile());
                    if (gallery != null) {
                        addFileToHistory(fc.getSelectedFile());
                        new GalleryBrowserWindow(gallery);
                        checkClose();
                    } else {
                        System.out.println("Couldn't load Gallery!");
                    }
                }

            }
        });
        toolbar.add(loadGalleryButton);

        JButton newGalleryButton = new JButton("New Gallery");
        newGalleryButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
                int returnVal = fc.showOpenDialog(window);
                if (returnVal == JFileChooser.APPROVE_OPTION) {
                    GalleryData gallery = new GalleryDataProvider().providerGet().createGallery(fc.getSelectedFile());
                    if (gallery != null) {
                        addFileToHistory(fc.getSelectedFile());
                        new GalleryBrowserWindow(gallery);
                        checkClose();
                    } else {
                        System.out.println("Couldn't load Gallery!");
                    }
                }

            }
        });
        toolbar.add(newGalleryButton);

        window.setLayout(new BorderLayout());
        window.add(toolbar, BorderLayout.NORTH);


        JPanel bottomBar = new JPanel();
        {
            shouldClose = new JCheckBox("Close On Selection?", true);
            bottomBar.setLayout(new BorderLayout());
            JButton about = new JButton("About / More Info");
            about.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent actionEvent) {
                    JOptionPane.showMessageDialog(bottomBar, aboutText,
                            "About FOG",
                            JOptionPane.INFORMATION_MESSAGE);
                }
            });
            bottomBar.add(about, BorderLayout.EAST);
            bottomBar.add(shouldClose, BorderLayout.WEST);
        }
        window.add(bottomBar, BorderLayout.SOUTH);

        JPanel recent = new JPanel();
        Border blackline = BorderFactory.createTitledBorder("Recent Galleries");
        recent.setBorder(blackline);
        recent.setMinimumSize(new Dimension(250, 250));
        window.add(recent, BorderLayout.CENTER);
        historyList = new JPanel();
        updateHistoryPanel();
        recent.add(historyList);
        window.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        window.pack();
        window.setMinimumSize(window.getSize());
        window.setVisible(true);
        EntryPoint.windowCount++;
        window.addWindowListener(new WindowListener() {
            @Override
            public void windowOpened(WindowEvent e) {

            }

            @Override
            public void windowClosing(WindowEvent e) {

                EntryPoint.windowCount--;
                EntryPoint.checkShouldClose();
            }

            @Override
            public void windowClosed(WindowEvent e) {

            }

            @Override
            public void windowIconified(WindowEvent e) {

            }

            @Override
            public void windowDeiconified(WindowEvent e) {

            }

            @Override
            public void windowActivated(WindowEvent e) {

            }

            @Override
            public void windowDeactivated(WindowEvent e) {

            }
        });
    }

    private static void addFileToHistory(File target) {
        GalleryData gallery = new GalleryDataProvider().providerGet().openGallery(target);

        if (gallery == null) {
            return; // no need to add a gallery that doesnt exist
        }
        ArrayList<File> oldHistory = history();
        for (File file : oldHistory) {
            if (file.equals(target)) {
                oldHistory.remove(target);  //remove it, so we add it to the start of the array
                break;
            }
        }
        oldHistory.add(0, target);
        if (oldHistory.size() > MAX_HISTORY) {
            oldHistory.remove(oldHistory.size() - 1);
        }
        saveHistory(oldHistory);
    }

    private static void saveHistory(ArrayList<File> files) {

        FileOutputStream out = null;
        try {
            historyStorage().createNewFile();
            out = new FileOutputStream(historyStorage());
            JsonAppendableWriter writer = JsonWriter.indent("\t").on(out);
            writer.array();
            {
                for (File file : files) {
                    writer.value(file.getAbsolutePath());
                }
            }
            writer.end();
            writer.done();
            out.flush();
            out.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static ArrayList<File> history() {
        ArrayList<File> results = new ArrayList<File>();
        if (!historyStorage().exists()) {
            return results;
        }
        try {
            Reader read = new FileReader(historyStorage());
            JsonArray fileArray = JsonParser.array().from(read);
            if (fileArray == null || fileArray.isEmpty()) {
                return results;
            }
            for (int i = 0; i < fileArray.size(); i++) {
                String element = fileArray.getString(i);
                if (element != null) {
                    results.add(new File(element));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return results;
    }

    public void checkClose() {

        if (shouldClose.isSelected()) {
            EntryPoint.checkShouldClose();
            window.setVisible(false);
            window.dispose();
        }

    }

    public static File historyStorage() {
        return new File(System.getProperty("user.dir") + File.separator + "openhistory.dat");
    }

    public void updateHistoryPanel() {
        historyList.removeAll();
        historyList.setLayout(new BoxLayout(historyList, BoxLayout.Y_AXIS));
        ArrayList<File> options = history();
        for (File element : options) {
            GalleryData gallery = new GalleryDataProvider().providerGet().openGallery(element);
            if (gallery != null) {
                String name = gallery.galleryNameGet();
                JButton fileButton = new JButton(name);
                fileButton.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent actionEvent) {
                        GalleryData gallery = new GalleryDataProvider().providerGet().openGallery(element);
                        new GalleryBrowserWindow(gallery);
                        checkClose();
                    }
                });

                historyList.add(fileButton);
            }
        }
    }
}