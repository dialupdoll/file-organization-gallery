package io.gitlab.dahlterm.FileOrganizationGallery.ui;

import io.gitlab.dahlterm.Async.TaskQueue;
import io.gitlab.dahlterm.FileOrganizationGallery.Toolbox;
import io.gitlab.dahlterm.FileOrganizationGallery.galleryabstractions.FileDisplay.Extensions.FileDisplayImageFile;
import io.gitlab.dahlterm.FileOrganizationGallery.other.SimpleCodeSnippet;

import javax.swing.*;
import java.awt.*;

public class StatusPanel implements SimpleCodeSnippet {
    private TaskQueue galleryTasks = null;

    private JProgressBar galleryTaskProgress = new JProgressBar();

    private JLabel statusArea = new JLabel("Status Text");
    JPanel statusComponent = new JPanel();

    public JComponent displayComponent() {
        statusComponent.setLayout(new BorderLayout());
        statusComponent.add(galleryTaskProgress, BorderLayout.EAST);

        statusArea.setHorizontalAlignment(JLabel.LEFT);
        // MIGHT STILL CHANGE TO RIGHT ALIGNMENT
        statusComponent.add(statusArea, BorderLayout.CENTER);
        galleryTaskProgress.setIndeterminate(false);
        galleryTaskProgress.setStringPainted(true);

        return statusComponent;
    }

    public void setTaskQueue(TaskQueue galleryWindowTasks) {
        if (galleryTasks != galleryWindowTasks) {
            /// remove from current monitor
            galleryTasks = galleryWindowTasks;
            galleryTasks.addStateChangeListener(this);
            FileDisplayImageFile.CachingImageHandler.fullImageQueue.
                    addStateChangeListener(this);
            FileDisplayImageFile.CachingImageHandler.slowerTaskQueue.
                    addStateChangeListener(this);
        }
    }

    int totalTasks = 0;
    int completedTasks = 0;
    int taskTypes = 0;
    String resultText = "";

    private void addTaskQueueData(TaskQueue queue, String panelShortText) {
        if (queue.isWorking()) {
            taskTypes++;
            totalTasks += queue.maxTasks();
            completedTasks += queue.completedTasks();
            if (!resultText.isBlank()) {
                resultText += ", ";
            }
            resultText += panelShortText;
        } else {
            queue.resetCounters();
        }
    }

    @Override
    public void update() {
        totalTasks = 0;
        completedTasks = 0;
        taskTypes = 0;
        resultText = "";
        addTaskQueueData(
                FileDisplayImageFile.CachingImageHandler.fullImageQueue,
                "loading images");
        addTaskQueueData(galleryTasks, "importing files");
        addTaskQueueData(
                FileDisplayImageFile.CachingImageHandler.slowerTaskQueue,
                "loading thumbnails");
        if (taskTypes != 0) {
            resultText = "[" + taskTypes + "] " + resultText;
        }

        statusArea.setText(resultText);
        galleryTaskProgress.setMaximum(totalTasks);
        galleryTaskProgress.setValue(completedTasks);
        if (totalTasks != 0) {
            galleryTaskProgress.setString(completedTasks + "/" + totalTasks);
        } else {
            galleryTaskProgress.setString("");
        }
        Toolbox.awtRepaint(galleryTaskProgress);
        Toolbox.awtRepaint(statusArea);
    }
}
