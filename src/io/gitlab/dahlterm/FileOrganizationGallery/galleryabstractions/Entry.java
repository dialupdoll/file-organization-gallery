package io.gitlab.dahlterm.FileOrganizationGallery.galleryabstractions;

import java.io.File;
import java.util.Map;
import java.util.Set;

public interface Entry {

    void removeEntry();

    void tagSet(String key);

    void tagClear(String key);

    boolean tagGet(String key);

    Set<String> tagList();

    void propertySet(String key, String value);

    String propertyGet(String key);

    Set<Map.Entry<String, String>> propertyList();

    File wrappedEntry();

    public boolean equalTo(Entry e);

}
