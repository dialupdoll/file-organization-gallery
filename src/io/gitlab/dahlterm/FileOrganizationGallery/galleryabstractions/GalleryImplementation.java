package io.gitlab.dahlterm.FileOrganizationGallery.galleryabstractions;

import java.io.File;

public interface GalleryImplementation {
    boolean hasGallery(File f);

    GalleryData createGallery(File f);

    GalleryData openGallery(File f);


}
