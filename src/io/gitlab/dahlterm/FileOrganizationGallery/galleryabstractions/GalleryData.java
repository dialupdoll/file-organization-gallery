package io.gitlab.dahlterm.FileOrganizationGallery.galleryabstractions;

import java.io.File;
import java.util.ArrayList;

public interface GalleryData {
    boolean galleryNameSet(String name);

    String galleryNameGet();

    ArrayList<Entry> entries();

    Entry addEntry(File f);

    boolean saveGallery(File f);

    boolean saveGallery();

    SearchOptions[] searchHandlers();

    ArrayList<Entry> search(SearchOptions options, String searchText);
}