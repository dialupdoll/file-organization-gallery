package io.gitlab.dahlterm.FileOrganizationGallery.galleryabstractions;

import com.grack.nanojson.*;
import io.gitlab.dahlterm.FileOrganizationGallery.Toolbox;
import io.gitlab.dahlterm.FileOrganizationGallery.galleryabstractions.tagging.Tag;

import javax.swing.*;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

public class GalleryDataProvider {
    public GalleryImplementation providerGet() {
        return new SimplisticGallery();
    }


    public class SimplisticGallery implements GalleryImplementation {
        public static final String GALLERY_TYPE = "gallery_format",
                GALLERY_SIMPLISTIC = "simplistic",
                GALLERY_TITLE_KEY = "gallery_title",
                GALLERY_HASH_KEY = "contained_files";

        public static final String ENTRY_EXTENSION = "file_extension";

        @Override
        public boolean hasGallery(File f) {
            try {
                if (f.isDirectory()) {
                    File head = new File(
                            f.getAbsolutePath() + "fog.meta.json");
                    if (!head.exists())
                        return false;
                    Reader read = new FileReader(f);
                    JsonObject metaFile = JsonParser.object().from(read);
                    if (metaFile.getString("identifier").
                            equals("simplistic"))
                        return true;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return false;
        }

        @Override
        public GalleryData createGallery(File f) {
            try {
                if (f.isDirectory()) {
                    File head = new File(f.getAbsolutePath()
                            + File.separator + "gallery.fog.json");
                    if (!head.exists()) {
                        FileOutputStream out = new FileOutputStream(head);
                        JsonAppendableWriter writer =
                                JsonWriter.indent("\t").on(out);
                        writer.object();
                        {
                            writer.value(GALLERY_TYPE, GALLERY_SIMPLISTIC);
                            writer.value(GALLERY_TITLE_KEY, "New Gallery");
                            writer.array(GALLERY_HASH_KEY);
                            {
                                // default to empty
                            }
                            writer.end();
                        }
                        writer.end();
                        writer.done();
                        out.flush();
                        out.close();

                        File rawEntries = new File(
                                f.getAbsolutePath()
                                        + File.separator + "raw");
                        rawEntries.mkdirs();

                        File metaEntries = new File(
                                f.getAbsolutePath()
                                        + File.separator + "meta");
                        metaEntries.mkdirs();

                    } else {
                        System.out.println("Gallery already exists here!");
                    }
                } else {
                    System.out.println("Not a directory!");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return openGallery(f);
        }

        @Override
        public GalleryData openGallery(File f) {
            try {
                if (f.isDirectory()) {
                    File head = new File(f.getAbsolutePath()
                            + File.separator + "gallery.fog.json");
                    if (!head.exists()) {
                        System.out.println(
                                "Couldn't open gallery - file not found! {"
                                        + head + "}");
                        return null;
                    }
                    Reader read = new FileReader(head);
                    JsonObject metaFile = JsonParser.object().from(read);
                    if (metaFile.getString(GALLERY_TYPE).
                            equals(GALLERY_SIMPLISTIC)) {
                        return new SimplisticGalleryData(head);
                    } else {
                        System.out.println(
                                "Couldn't open gallery - wrong gallery type!");
                    }
                } else {
                    System.out.println(
                            "Couldn't open gallery, is not a directory!");
                }
            } catch (Exception e) {
                JOptionPane.showMessageDialog(
                        null,
                        e.getStackTrace(), "Error" +
                                " opening gallery.", JOptionPane.ERROR_MESSAGE);
                System.out.println("Attempted to show dialog.");
                e.printStackTrace();
            } catch (Throwable e) {

                JOptionPane.showMessageDialog(
                        null, e.getStackTrace(),
                        "Error opening gallery.",
                        JOptionPane.ERROR_MESSAGE);
                e.printStackTrace();
            }
            return null;
        }


    }

    public class SimplisticGalleryData implements GalleryData {
        File indexFile = null;

        SimplisticGalleryData(File target) {
            try {
                indexFile = target;
                Reader read = new FileReader(indexFile);
                JsonObject data = JsonParser.object().from(read);
                /// todo: sanitization
            } catch (Exception e) {
                e.printStackTrace();
            }
        }


        private static File
        rawFile(File indexFile, UUID ident, String extension) {

            File result = new File(indexFile.getParent() +
                    File.separator + "raw" + File.separator + ident.toString()
                    + "." + extension);
            return result;
        }

        private static File metaFileFromID(UUID ident, File indexFile) {

            File result = new File(indexFile.getParent() +
                    File.separator + "meta" + File.separator + ident.toString()
                    + ".meta.json");
            return result;
        }

        public static String getExtension(UUID ident, File indexFile) {

            Reader read = null;
            try {
                read = new FileReader(metaFileFromID(ident, indexFile));
                JsonObject data = JsonParser.object().from(read);
                return data.getString(SimplisticGallery.ENTRY_EXTENSION);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        private static File metaFile(UUID ident, File indexFile) {

            File result = new File(indexFile.getParent() +
                    File.separator + "meta" + File.separator +
                    ident.toString() + ".meta.json");
            return result;
        }

        @Override
        public boolean galleryNameSet(String name) {
            try {
                indexFile = indexFile;
                Reader read = new FileReader(indexFile);
                JsonObject data = JsonParser.object().from(read);
                data.put(SimplisticGallery.GALLERY_TITLE_KEY, name);

                FileWriter fileWriter = new FileWriter(indexFile, false);
                JsonAppendableWriter output =
                        JsonWriter.indent("\t").on(fileWriter);
                output.object(data);    /// this line does the legwork
                //                          of re-outputting the json
                output.done();
                fileWriter.flush();
                fileWriter.close();
                return true;
            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("Could not save Gallery Name");
            }
            return false;
        }

        @Override
        public String galleryNameGet() {
            try {
                indexFile = indexFile;
                Reader read = new FileReader(indexFile);
                JsonObject data = JsonParser.object().from(read);
                return data.getString(SimplisticGallery.GALLERY_TITLE_KEY);
            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("Could not get Gallery Name");
            }
            return "Could not get Gallery Name";
        }

        @Override
        public ArrayList<Entry> entries() {

            Reader read = null;
            try {
                read = new FileReader(indexFile);
                JsonObject data = JsonParser.object().from(read);
                read.close();
                JsonArray keys =
                        data.getArray(SimplisticGallery.GALLERY_HASH_KEY);
                ArrayList<Entry> entries = new ArrayList<Entry>();
                for (int i = 0; i < keys.size(); i++) {

                    UUID uid = UUID.fromString(keys.getString(i));
                    entries.add(getEntry(uid));
                }
                return entries;
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (JsonParserException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }


        public void remove(SimplisticGalleryEntry e) {
            Reader read = null;
            try {
                read = new FileReader(indexFile);
                JsonObject data = JsonParser.object().from(read);
                read.close();
                JsonArray keys =
                        data.getArray(SimplisticGallery.GALLERY_HASH_KEY);
                keys.remove(e.id.toString());

                FileWriter fileWriter =
                        new FileWriter(indexFile, false);
                JsonAppendableWriter output =
                        JsonWriter.indent("\t").on(fileWriter);
                output.object(data);    /// this line does the legwork
                //                          of re-outputting the json
                output.done();
                fileWriter.flush();
                fileWriter.close();

                File metaFile = metaFile(e.id, e.index);
                e.wrappedEntry().delete();
                metaFile.delete();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }

        @Override
        public Entry addEntry(File target) {
            UUID addition = UUID.randomUUID();
            try {
                /// below: create entry meta file
                Optional<String> extensionResult =
                        Toolbox.getExtensionByStringHandling(target.getPath());
                if (extensionResult.isEmpty()) {
                    System.err.println("Couldn't get extension");
                } else {
                    /// below delegates the default meta file JSON out for conciseness
                    createEntryMetaFile(addition, extensionResult.get());
                    /// ----
                    Files.copy(Paths.get(target.getAbsolutePath()),
                            Paths.get(rawFile(indexFile,
                                    addition, extensionResult.get())
                                    .getAbsolutePath()));
                    /// above copies into raw folder
                    /// TODO: add progress bar / spinner for above
                    /// below: add to entry UID array in head meta file
                    //indexFile = target;
                    Reader read = new FileReader(indexFile);
                    JsonObject data = JsonParser.object().from(read);
                    read.close();
                    JsonArray keys = data.getArray(SimplisticGallery.
                            GALLERY_HASH_KEY);
                    keys.add(addition.toString());

                    FileWriter fileWriter = new FileWriter(indexFile,
                            false);
                    JsonAppendableWriter output = JsonWriter.
                            indent("\t").on(fileWriter);
                    output.object(data);    /// this line does the legwork
                    // of re-outputting the json
                    output.done();
                    fileWriter.flush();
                    fileWriter.close();
                    return getEntry(addition);
                }
            } catch (Exception e) {
                System.out.println("Could not copy file into gallery!");
                e.printStackTrace();
            }
            return null;
        }

        private Entry getEntry(UUID id) {
            return new SimplisticGalleryEntry(metaFile(id, indexFile),
                    indexFile, id, this);
        }

        private void createEntryMetaFile(UUID id, String extension) {
            FileOutputStream out = null;
            try {
                out = new FileOutputStream(
                        metaFile(id, indexFile), false);
                JsonAppendableWriter writer = JsonWriter.indent("\t").on(out);
                writer.object();
                {
                    writer.value(SimplisticGallery.ENTRY_EXTENSION, extension);
                    writer.array(SimplisticGalleryEntry.TAG_ARRAY_KEY);
                    writer.value("untagged");
                    writer.end();
                }
                writer.end();
                writer.done();
                out.flush();
                out.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public boolean saveGallery(File f) {
            /// not built to cache changes, auto saves as progresses
            return false;
        }

        @Override
        public boolean saveGallery() {
            /// not built to cache changes, auto saves as progresses
            return false;
        }

        final String RandomSort = "Random Sort";
        final String DefaultSort = "Default Sort";
        final String TagSort = "Tag Sort";
        final SearchOptions randomSearch = SearchOptions
                .build(RandomSort)
                .complete();
        final String resultOrder = "Order";
        final String orderDescending = "Descending";
        final String orderAscending = "Ascending";
        final SearchOptions defaultSearch = SearchOptions
                .build(DefaultSort)
                .addRadioOption(resultOrder,
                        new String[]{
                                orderAscending, orderDescending
                        },
                        orderDescending)
                .complete();
        final SearchOptions tagSearch = SearchOptions
                .build(TagSort)
                .addRadioOption(resultOrder,
                        new String[]{
                                orderAscending, orderDescending
                        },
                        orderAscending)
                .complete();

        @Override
        public SearchOptions[] searchHandlers() {
            return new SearchOptions[]{
                    defaultSearch, randomSearch, tagSearch
            };
        }

        @Override
        public ArrayList<Entry> search(SearchOptions options,
                                       String searchText) {
            ArrayList<Entry> result = new ArrayList<>();
            ArrayList<Tag> tags =
                    Tag.tagListFromCommaBasedSearchString(searchText);
            if (tags != null) {
                if (!tags.isEmpty()) {
                    for (Entry e : entries()) {
                        boolean shouldAdd = true;
                        for (Tag t : tags) {
                            if (t.isAvoidRequest) {
                                if (t.matchesTagContent(e)) {
                                    shouldAdd = false;
                                }
                            } else {
                                if (!t.matchesTagContent(e)) {
                                    shouldAdd = false;
                                }
                            }
                        }
                        if (shouldAdd) {
                            result.add(e);
                        }
                    }
                } else {
                    result = entries();
                }
            } else {
                result = entries();
            }
            if (options.equals(randomSearch)) {
                Collections.shuffle(result);
            } else if (options.equals(tagSearch)) {
                Collections.sort(result, new Comparator<Entry>() {
                    @Override
                    public int compare(Entry entry, Entry t1) {
                        Integer l, r;
                        l = entry.tagList().size();
                        r = t1.tagList().size();
                        return l.compareTo(r);
                    }
                });
                if (options.getSelection(resultOrder).equals(orderDescending)) {
                    Collections.reverse(result);
                }
            } else {
                if (options.equals(defaultSearch)) {
                    if (options.getSelection(resultOrder).equals(orderAscending)) {
                        Collections.reverse(result);
                    }
                }
            }
            return result;
        }
    }

    public class SimplisticGalleryEntry implements Entry {
        public static final String TAG_ARRAY_KEY = "entry_flag_tags";
        File metaID = null;
        File index;
        UUID id;
        SimplisticGalleryData container;

        public SimplisticGalleryEntry(File target, File indexFile, UUID uid,
                                      SimplisticGalleryData owner) {
            metaID = target;
            index = indexFile;
            id = uid;
            container = owner;
        }

        @Override
        public void removeEntry() {
            container.remove(this);
        }

        @Override
        public void tagSet(String key) {
            try {
                Reader read = new FileReader(metaID);
                JsonObject data = JsonParser.object().from(read);
                JsonArray tagFlags = data.getArray(TAG_ARRAY_KEY);
                if (tagFlags.contains(key)) {
                    /// we're done here, it's already in the file
                    return;
                } else {
                    tagFlags.add(key);
                    FileWriter fileWriter =
                            new FileWriter(metaID, false);

                    JsonAppendableWriter output =
                            JsonWriter.indent("\t").on(fileWriter);
                    output.object(data);    /// this line does the legwork
                    //                          of re-outputting the json
                    output.done();
                    fileWriter.flush();
                    fileWriter.close();
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void tagClear(String key) {
            try {
                Reader read = new FileReader(metaID);
                JsonObject data = JsonParser.object().from(read);
                JsonArray tagFlags = data.getArray(TAG_ARRAY_KEY);
                if (tagFlags.contains(key)) {
                    tagFlags.remove(key);
                    FileWriter fileWriter =
                            new FileWriter(metaID, false);

                    JsonAppendableWriter output =
                            JsonWriter.indent("\t").on(fileWriter);
                    output.object(data);    /// this line does the legwork
                    //                          of re-outputting the json
                    output.done();
                    fileWriter.close();
                } else {
                    /// we're done here, it's already not in the file
                    return;
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public boolean tagGet(String key) {
            try {
                Reader read = new FileReader(metaID);
                JsonObject data = JsonParser.object().from(read);
                JsonArray tagFlags = data.getArray(TAG_ARRAY_KEY);
                return tagFlags.contains(key);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return false;
        }

        @Override
        public void propertySet(String key, String value) {
            System.err.println("Not implemented!!!");
        }

        @Override
        public String propertyGet(String key) {
            System.err.println("Not implemented!!!");
            return null;
        }

        @Override
        public Set<Map.Entry<String, String>> propertyList() {
            System.err.println("Not implemented!!!");
            return null;
        }

        @Override
        public File wrappedEntry() {
            return SimplisticGalleryData.rawFile(index, id,
                    SimplisticGalleryData.getExtension(id, index));
        }

        @Override
        public boolean equalTo(Entry e) {
            return e.wrappedEntry().equals(wrappedEntry());
        }

        @Override
        public Set<String> tagList() {
            Set<String> result = new HashSet<String>();
            try {
                Reader read = new FileReader(metaID);
                JsonObject data = JsonParser.object().from(read);
                JsonArray tagFlags = data.getArray(TAG_ARRAY_KEY);
                if (tagFlags == null) {
                    return null;
                }
                if (tagFlags.isEmpty()) {
                    return null;
                }
                Iterator<Object> i = tagFlags.iterator();
                while (i.hasNext()) {
                    Object o = i.next();
                    if (o instanceof String) {
                        String s = (String) o;
                        result.add(s);
                    } else {
                        System.out.println(
                                "Array contains non-strings, weird.");
                    }
                }
                return result;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
    }
}
