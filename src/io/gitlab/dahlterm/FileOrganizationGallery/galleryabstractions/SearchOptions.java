package io.gitlab.dahlterm.FileOrganizationGallery.galleryabstractions;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.Locale;

public class SearchOptions {
    private String name = null;
    private State options[] = null;

    public Boolean getChecked(String name) {
        name = name.toLowerCase(Locale.ROOT).trim();
        for (State iterator : options) {
            if (iterator.name().toLowerCase(Locale.ROOT).trim().equals(name)) {
                return iterator.checkboxState();
            }
        }
        return null;
    }

    public String getSelection(String name) {
        name = name.toLowerCase(Locale.ROOT).trim();
        for (State iterator : options) {
            if (iterator.name().toLowerCase(Locale.ROOT).trim().equals(name)) {
                return iterator.radioState();
            }
        }
        return null;
    }

    public static abstract class State {
        enum Type {
            Radio, Checkbox
        }

        abstract String name();

        abstract Type stateType();

        abstract String radioState();

        abstract boolean checkboxState();

        abstract String eitherState();

        abstract public JComponent uiComponent();

        public static State createCheckboxOption(String checkboxName,
                                                 boolean defaultOption) {
            return new State() {
                JCheckBox component = null;
                JPanel uiComponent = new JPanel();
                Boolean currentState = null;

                @Override
                String name() {
                    return checkboxName;
                }

                @Override
                Type stateType() {
                    return Type.Checkbox;
                }

                @Override
                String radioState() {
                    String justification = "Cannot get the radio " +
                            "state of a checkbox option";
                    throw new TypeNotPresentException(justification,
                            new Exception(justification));
                }

                @Override
                boolean checkboxState() {
                    if (component != null) {
                        return component.isSelected();
                    }
                    if (currentState != null) {
                        return currentState;
                    }
                    return false;
                }

                @Override
                String eitherState() {
                    if (checkboxState()) {
                        return "true";
                    } else {
                        return "false";
                    }
                }

                @Override
                public JComponent uiComponent() {
                    if (component != null) {
                        return uiComponent;
                    } else {
                        component = new JCheckBox(checkboxName);
                        uiComponent.setLayout(new BorderLayout());
                        uiComponent.add(component, BorderLayout.CENTER);
                        uiComponent.setBorder(
                                BorderFactory.
                                        createEmptyBorder(
                                                2, 2,
                                                0, 2));
                        if (currentState != null) {
                            component.setSelected(currentState);
                        }
                        return uiComponent;
                    }
                }
            };
        }

        public static State createRadioOption(String radioName,
                                              String[] radioOptions,
                                              String defaultOption) {

            return new State() {
                String currentState = defaultOption;
                JComboBox component = null;
                JPanel uiComponent = new JPanel();

                @Override
                String name() {
                    return radioName;
                }

                @Override
                Type stateType() {
                    return Type.Radio;
                }

                @Override
                String radioState() {
                    if (component != null) {
                        return (String) component.getItemAt(
                                component.getSelectedIndex());
                    } else {
                        return currentState;
                    }
                }

                @Override
                boolean checkboxState() {
                    String justification = "Cannot get the checkbox " +
                            "state of a radio option";
                    throw new TypeNotPresentException(justification,
                            new Exception(justification));
                }

                @Override
                String eitherState() {
                    return radioState();
                }

                @Override
                public JComponent uiComponent() {
                    if (component != null) {

                        return uiComponent;
                    } else {
                        component = new JComboBox(radioOptions);
                        uiComponent.setLayout(new BorderLayout());
                        uiComponent.add(component, BorderLayout.CENTER);
                        uiComponent.setBorder(
                                BorderFactory.
                                        createEmptyBorder(
                                                2, 2,
                                                0, 2));
                        if (currentState != null) {
                            component.setSelectedItem(currentState);
                        } else if (defaultOption != null) {
                            component.setSelectedItem(defaultOption);
                        } else {
                            component.setSelectedIndex(0);
                        }
                        return uiComponent;
                    }
                }
            };

        }
    }

    public String getName() {
        return name;
    }

    public State[] getOptions() {
        return options;
    }

    public interface Constructor {
        Constructor addRadioOption(String radioName,
                                   String[] radioOptions,
                                   String defaultOption);

        Constructor addCheckboxOption(String checkboxName,
                                      boolean defaultOption);

        SearchOptions complete();
    }

    public static Constructor build(String searchName) {
        return new Constructor() {
            String resultName = searchName;
            ArrayList<State> results = new ArrayList<>();

            @Override
            public Constructor addRadioOption(String radioName, String[] radioOptions, String defaultOption) {
                results.add(
                        State.createRadioOption(
                                radioName, radioOptions, defaultOption
                        ));
                return this;
            }

            @Override
            public Constructor addCheckboxOption(String checkboxName, boolean defaultOption) {
                results.add(
                        State.createCheckboxOption(
                                checkboxName, defaultOption
                        ));
                return this;
            }

            @Override
            public SearchOptions complete() {
                SearchOptions result = new SearchOptions();
                result.options = results.toArray(new State[1]);
                result.name = resultName;
                return result;
            }
        };
    }


}
