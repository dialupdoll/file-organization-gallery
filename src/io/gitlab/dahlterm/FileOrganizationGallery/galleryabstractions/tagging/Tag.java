package io.gitlab.dahlterm.FileOrganizationGallery.galleryabstractions.tagging;

import io.gitlab.dahlterm.FileOrganizationGallery.galleryabstractions.Entry;
import io.gitlab.dahlterm.Logging;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Set;

public class Tag {
    String tagName;
    public boolean isAvoidRequest = false;

    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Tag))
            return false;
        Tag otherTag = (Tag) other;
        boolean equivalentText = otherTag.tagName.toLowerCase(Locale.ROOT)
                .equals(tagName.toLowerCase(Locale.ROOT));
        if (equivalentText) //&& (otherTag.isAvoidRequest == this.isAvoidRequest)
            return true;
        else
            return false;
    }

    public static Tag fromString(String name) {
        Tag result = new Tag(name);
        if (result.tagName.isBlank()) {
            return null;
        }
        return result;
    }

    private Tag(String name) {
        String resultingTag = name.trim().
                toLowerCase(Locale.ROOT);
        if (resultingTag.charAt(0) == '-') {
            resultingTag = resultingTag.substring(1);
            isAvoidRequest = true;
            resultingTag = resultingTag.trim().
                    toLowerCase(Locale.ROOT);
            if (resultingTag.isBlank()) {
                ///handled in fromString()
            }
        }
        tagName = resultingTag;
    }

    public boolean matchesTagContent(Entry e) {
        return e.tagGet(tagName);
    }

    public JComponent tagComponent(TagView owner, boolean isEntryTag,
                                   boolean isResultTag, boolean isSearchTag) {
        {
            /// sanitization
            boolean isAlreadyATypeOfTag = false;
            if (isEntryTag) {
                isAlreadyATypeOfTag = true;
            }
            if (isResultTag) {
                if (isAlreadyATypeOfTag) {
                    System.err.println("Can't be two types of tag!");
                } else {
                    isAlreadyATypeOfTag = true;
                }
            }
            if (isSearchTag) {
                if (isAlreadyATypeOfTag) {
                    System.err.println("Can't be two types of tag!");
                } else {
                    isAlreadyATypeOfTag = true;
                }
            }
        }

        Tag self = this;
        JPanel result = new JPanel();
        //result.setAlignmentX(Component.RIGHT_ALIGNMENT);
        result.setLayout(new BorderLayout());
        JLabel tagNameLabel = new JLabel(tagName);
        if (this.isAvoidRequest) {
            tagNameLabel.setText("- " + tagNameLabel.getText());
        }

        tagNameLabel.setBorder(BorderFactory.createEmptyBorder(
                0, 10, 0, 10));
        result.add(tagNameLabel, BorderLayout.CENTER);
        tagNameLabel.setHorizontalAlignment(SwingConstants.TRAILING);
        result.setAlignmentX(Component.RIGHT_ALIGNMENT);

        // TODO: fix the broken remove-tag-issue (sometimes doesnt refresh
        //  search?)
        JButton removeTag = new JButton("✘");
        if (isEntryTag) {

            // TODO: edit button
            removeTag.addActionListener(actionEvent -> {
                if (owner.getCurrentlySelectedEntry() != null) {
                    owner.getCurrentlySelectedEntry().tagClear(tagName);
                    owner.updateEntryTags();
                }
            });
            result.add(removeTag, BorderLayout.EAST);
        }
        if (isSearchTag) {
            removeTag.addActionListener(actionEvent -> owner.removeTagFromSearch(self));
            result.add(removeTag, BorderLayout.EAST);
        }
        if (isResultTag) {
            // TODO: better + icon (not text)
            JPanel optionsContainer = new JPanel();
            {
                optionsContainer.setLayout(new GridLayout(0, 2));

                JButton addToSearch = new JButton("+");
                addToSearch.addActionListener(actionEvent -> {
                    owner.addTagToSearch(self);
                });
                optionsContainer.add(addToSearch);

                JButton addAvoidToSearch = new JButton("-");
                addAvoidToSearch.addActionListener(actionEvent -> {
                    owner.addAvoidTag(self);
                });
                optionsContainer.add(addAvoidToSearch);
            }
            result.add(optionsContainer, BorderLayout.EAST);
        }
        return result;
    }

    /// BELOW IS ALL STATIC FUNCTIONS

    public static ArrayList<Entry> searchResultsFromTagList(
            ArrayList<Tag> tags,
            ArrayList<Entry> entries, boolean requireAllTags) {
        ArrayList<Entry> result = new ArrayList<>();

        for (Entry entryIterator : entries) {
            boolean shouldAddEntry = false;
            boolean vetoEntryWithAvoidRequest = false;
            if (requireAllTags) {
                shouldAddEntry = true;
            }
            for (Tag tagIterator : tags) {
                if (requireAllTags) {
                    /// true until false - checking until find an avoid
                    //  request or a non-matching-tag-to-entry
                    if (!tagIterator.matchesTagContent(entryIterator)) {
                        shouldAddEntry = false;
                    }
                } else {
                    /// false until true - checking to see if there is a
                    //  reason to include it, a matching tag
                    if (tagIterator.matchesTagContent(entryIterator)) {
                        shouldAddEntry = true;
                    }
                }
                if (tagIterator.matchesTagContent(entryIterator)) {
                    if (tagIterator.isAvoidRequest) {
                        vetoEntryWithAvoidRequest = true;
                    }
                }
            }
            if (shouldAddEntry) {
                if (!vetoEntryWithAvoidRequest) {
                    result.add(entryIterator);
                }
            }
        }
        if (result.isEmpty()) {
            return null;
        } else {
            return result;
        }
    }

    public static ArrayList<Tag> tagListFromCommaBasedSearchString(String search) {
        ArrayList<Tag> results = new ArrayList<>();
        if (search.contains(",")) {         /// MULTIPLE ENTRIES
            String[] tags = search.split(",");
            for (String tag : tags) {
                tag = tag.trim();
                tag = tag.toLowerCase(Locale.ROOT);
                if (!tag.isEmpty()) {
                    results.add(new Tag(tag));
                }
            }
        } else {                            /// ONLY ONE ENTRY
            search = search.trim();
            search = search.toLowerCase(Locale.ROOT);
            if (!search.isBlank()) {
                results.add(new Tag(search));
            }
        }
        if (results.isEmpty())
            return null;
        else
            return results;
    }

    public static ArrayList<Tag> fromEntry(Entry e) {
        if (e == null)
            return null;

        ArrayList<Tag> result = new ArrayList<Tag>();

        Set<String> tags = e.tagList();

        if (tags == null)
            return null;

        for (String s : tags) {
            result.add(new Tag(s));
        }

        if (result.isEmpty()) {
            Logging.log(Logging.FLOW,
                    "Really unsure how you got here. Tag " +
                            "fromEntry function says wtf??");
            return null;
        } else {
            return result;
        }
    }

    public static ArrayList<Tag> fromSearchResultEntries(ArrayList<Entry> entries, ArrayList<Tag> alreadyPresent) {
        ArrayList<Tag> result = new ArrayList<Tag>();

        /// for all entries
        for (Entry entryIterator : entries) {
            /// get the tags
            ArrayList<Tag> entryTags = fromEntry(entryIterator);
            /// iterate through the tags
            if (entryTags != null) {
                for (Tag tagIterator : entryTags) {
                    /// if not already in list of results, add it
                    if (!result.contains(tagIterator)) {
                        result.add(tagIterator);
                    }
                }
            }
        }

        /// return null if no result, signaling an error to recieving code
        if (result.isEmpty()) {
            return null;
        } else {
            return result;
        }
    }
}
