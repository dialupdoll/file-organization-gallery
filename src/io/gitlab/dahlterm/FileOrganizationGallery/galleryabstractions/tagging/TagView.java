package io.gitlab.dahlterm.FileOrganizationGallery.galleryabstractions.tagging;

import io.gitlab.dahlterm.FileOrganizationGallery.Toolbox;
import io.gitlab.dahlterm.FileOrganizationGallery.galleryabstractions.Entry;
import io.gitlab.dahlterm.FileOrganizationGallery.galleryabstractions.GalleryData;
import io.gitlab.dahlterm.FileOrganizationGallery.galleryabstractions.SearchOptions;
import io.gitlab.dahlterm.FileOrganizationGallery.other.SimpleCodeSnippet;

import javax.swing.*;
import javax.swing.border.BevelBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.util.ArrayList;
import java.util.Locale;

public class TagView extends JPanel {
    final String prompt = "Enter Search Term";
    SimpleCodeSnippet parentOnSearchEvent;
    private boolean isViewingEntry = false;
    JPanel secondaryTagsPanel;
    /// selection tags is either the entry tags or
    // the tags for the search results
    JPanel searchTagsPanel;
    private JTextArea searchText;
    JButton addTagButton = null;
    GalleryData galleryData = null;
    final TagView self = this;

    private Entry currentlySelectedEntry = null;
    JComponent tagAddingInProcess = null;

    private ArrayList<Tag> currentSearchTags = new ArrayList<>();
    private ArrayList<Tag> currentSearchResultsTags = null;
    private ArrayList<Entry> lastSearchResultEntries = null;

    private boolean isCommaBased() {
        return true;
    }

    /// exclusive searches do not allow any entries that don't have all tags
    // (this is the default for most boorus)

    private boolean isExclusiveSearch() {
        return true;
    }

    public boolean isViewingEntry() {
        return currentlySelectedEntry != null;
    }

    public boolean isViewingThumbnailSearch() {
        return !isViewingEntry();
    }

    public Entry getCurrentlySelectedEntry() {
        return currentlySelectedEntry;
    }

    public void setCurrentlySelectedEntry(Entry e) {
        if (e != null) {
            editButton.setEnabled(true);
            addButton.setEnabled(true);
        } else {
            editButton.setEnabled(false);
            addButton.setEnabled(false);
            saveChanges.setEnabled(false);
        }
        currentlySelectedEntry = e;
        tagAddingInProcess = null;
        updateEntryTags();
    }

    public String getSearch() {
        if (searchText.getText().equals(prompt)) {
            return "";
        }
        if (searchText.getText().isBlank())
            return "";
        return searchText.getText();
    }

    private void updateSearchFrom(ArrayList<Tag> tags) {
        String resultSearchText = "";
        for (Tag iterator : tags) {
            String addition = "";
            if (!resultSearchText.isBlank()) {
                addition = ", ";
            }
            if (iterator.isAvoidRequest) {
                addition += "-";
            }
            addition += iterator.tagName;
            resultSearchText += addition;
        }
        setSearch(resultSearchText, currentSearchSettings());
        onSearchInitiated();
    }

    public void removeTagFromSearch(Tag t) {
        if (currentSearchTags == null) {
            return;
        }
        if (currentSearchTags.isEmpty()) {
            return;
        }
        Tag target = null;
        t = Tag.fromString(t.tagName);
        t.isAvoidRequest = false;
        for (Tag tagIterator : currentSearchTags) {
            if (tagIterator.tagName.equals(t.tagName)) {
                target = tagIterator;
            }
        }
        currentSearchTags.remove(target);

        updateSearchFrom(currentSearchTags);
    }

    public void addTagToSearch(Tag t) {
        updateSearchTags();
        if (currentSearchTags == null) {
            currentSearchTags = new ArrayList<>();
        }
        t.isAvoidRequest = false;
        if (currentSearchTags.contains(t)) {
            Tag inverse = t;
            t.isAvoidRequest = true;
            currentSearchTags.remove(t);
        }
        currentSearchTags.add(t);
        updateSearchFrom(currentSearchTags);
    }

    public void addAvoidTag(Tag t) {
        updateSearchTags();
        if (currentSearchTags == null) {
            currentSearchTags = new ArrayList<>();
        }
        if (currentSearchTags.contains(t)) {
            currentSearchTags.remove(t);
            Tag inverse = t;
            inverse.isAvoidRequest = !inverse.isAvoidRequest;
            currentSearchTags.remove(t);
        }
        t.isAvoidRequest = true;
        currentSearchTags.add(t);
        updateSearchFrom(currentSearchTags);
    }

    public void onSearchInitiated() {
        updateSearchTags();

        parentOnSearchEvent.update();
        if (isViewingEntry && isViewingThumbnailSearch()) {
            System.err.println("Can't be in both entry and thumbnail search " +
                    "view mode at the same time");
            return;
        }
        if (isViewingEntry()) {
            updateEntryTags();
        }
        if (isViewingThumbnailSearch()) {
            updateResultTags();
        }
    }

    public void setSearch(String search, SearchOptions newOptions) {
        searchText.setText(search);
        searchOptionsSelector.setSelectedItem(newOptions.getName());
        onSearchOptionSelect();
        updateSearchTags();
        current = newOptions;

        if (isViewingThumbnailSearch()) {
            updateResultTags();
        }
        if (isViewingEntry()) {
            updateEntryTags();
        }
    }

    /// SUB SEARCH INITIATION SECTORS
    private void updateSearchTags() {
        if (currentSearchTags != null) {
            currentSearchTags.clear();
        } else {
            currentSearchTags = new ArrayList<>();
            onSearchOptionSelect();
            /// TODO: I don't fuckin get why this works but it does
            // makes the search options show without a click
        }
        String search = getSearch();
        /// might need to sanitize search string further than getSearch() does
        currentSearchTags = Tag.tagListFromCommaBasedSearchString(search);

        searchTagsPanel.removeAll();
        if (currentSearchTags != null) {
            if (!currentSearchTags.isEmpty()) {
                for (Tag iterator : currentSearchTags) {
                    searchTagsPanel.add(iterator.tagComponent(
                            this, false,
                            false, true));
                }
            }
        }
        Toolbox.awtRepaint(searchTagsPanel);
    }

    private void updateResultTags() {
        if (currentSearchResultsTags != null) {
            ArrayList<Tag> results = Tag.fromSearchResultEntries(
                    lastSearchResultEntries,
                    currentSearchTags);
            if (results == null) {
                return;
            }
            secondaryTagsPanel.removeAll();
            for (Tag iterator : results) {
                secondaryTagsPanel.add(iterator.tagComponent(this, false, true,
                        false));
            }
            Toolbox.awtRepaint(secondaryTagsPanel);
        }
    }

    public void updateEntryTags() {
        /// this is public to allow parent to update when a new entry is
        // selected, thus changing the entry tag section without changing
        // search tag section
        secondaryTagsPanel.removeAll();
        if (currentlySelectedEntry != null) {
            ArrayList<Tag> list = Tag.fromEntry(currentlySelectedEntry);
            if (list != null) {
                for (Tag t : list) {
                    secondaryTagsPanel.add(t.tagComponent(this, true, false,
                            false));
                }
            }

        } else {

        }
        if (tagAddingInProcess != null) {
            addTagButton.setEnabled(false);
        } else {
            addTagButton.setEnabled(true);
        }
        Toolbox.awtRepaint(secondaryTagsPanel);
    }
    /// SEARCH INIT CODE END

    JButton editButton, addButton, saveChanges;

    private static JComponent tagEditor(TagView parent) {
        JPanel editor = new JPanel();
        editor.setLayout(new BorderLayout());
        JTextField editBox = new JTextField();
        //editBox.setWrapStyleWord(true);
        //editBox.setLineWrap(true);
        editBox.setActionCommand("Tag Name");
        editBox.setAlignmentY(0.5f);
        editBox.setHorizontalAlignment(SwingConstants.HORIZONTAL);
        editBox.setBorder(BorderFactory.createBevelBorder(BevelBorder.LOWERED));
        editor.add(editBox, BorderLayout.CENTER);
        JPanel actions = new JPanel();
        actions.setLayout(new FlowLayout());
        JButton confirm = new JButton("✓");
        confirm.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                String resultingTag = editBox.getText().trim().
                        toLowerCase(Locale.ROOT);
                if (resultingTag.charAt(0) == '-') {
                    resultingTag = resultingTag.substring(1);
                }
                parent.currentlySelectedEntry.tagSet(resultingTag);
                parent.tagAddingInProcess = null;
                parent.updateEntryTags();
            }
        });
        actions.add(confirm);
        JButton cancel = new JButton("✘");
        cancel.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                parent.tagAddingInProcess = null;
                parent.updateEntryTags();
            }
        });
        actions.add(cancel);
        editor.add(actions, BorderLayout.EAST);
        return editor;
    }

    JComboBox searchOptionsSelector = null;
    JPanel searchOptionsArea = null;

    SearchOptions current = null;

    public SearchOptions currentSearchSettings() {
        SearchOptions result = null;
        String target =
                searchOptionsSelector.getSelectedItem().toString();
        for (SearchOptions searchOptionsIterator :
                galleryData.searchHandlers()) {
            if (searchOptionsIterator.getName().equals(target)) {
                result = searchOptionsIterator;
            }
        }
        return result;
    }

    void onSearchOptionSelect() {
        String target =
                searchOptionsSelector.getSelectedItem().toString();

        searchOptionsArea.removeAll();
        SearchOptions result = currentSearchSettings();
        if (result != null) {

            for (SearchOptions.State stateIterator :
                    result.getOptions()) {
                if (stateIterator != null) {
                    searchOptionsArea.add(stateIterator.uiComponent());

                }
            }
            Toolbox.awtRepaint(searchOptionsArea);
        } else {
            System.out.println("Error in search options!");
        }
    }

    public TagView(boolean isViewingEntry, SimpleCodeSnippet updater,
                   GalleryData galleryDataProvided) {
        TagView parentTV = this;
        this.isViewingEntry = isViewingEntry;
        parentOnSearchEvent = updater;
        galleryData = galleryDataProvided;

        setLayout(new BorderLayout());
        JPanel searchBar = new JPanel();
        {
            searchBar.setLayout(new BorderLayout());

            searchText = new JTextArea(prompt);
            {
                searchText.setWrapStyleWord(true);
                searchText.setLineWrap(true);
                searchText.setBorder(BorderFactory.createBevelBorder(BevelBorder.LOWERED));


                searchText.addFocusListener(new FocusListener() {
                    @Override
                    public void focusGained(FocusEvent focusEvent) {
                        if (searchText.getText().equals(prompt)) {
                            searchText.setText("");
                        }
                    }

                    @Override
                    public void focusLost(FocusEvent focusEvent) {
                        if (searchText.getText().trim().isEmpty()) {
                            searchText.setText(prompt);
                        }
                    }
                });

                JPanel padding = new JPanel();
                padding.setBorder(BorderFactory.createEmptyBorder(4, 4, 4, 4));
                padding.add(searchText);

                searchText.setMinimumSize(new Dimension(20, 20));
                //searchText.setColumns(12);
                padding.setMinimumSize(new Dimension(25, 25));

                searchBar.add(padding, BorderLayout.CENTER);
                Toolbox.swingThreadSafeMessage(() -> {
                    Toolbox.awtRepaint(self);
                });
            }

            JButton searchButton = new JButton(">");
            {
                searchBar.add(searchButton, BorderLayout.EAST);

                searchButton.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent actionEvent) {
                        onSearchInitiated();
                        /// above calls parent update event as well
                    }
                });
            }

            searchOptionsArea = new JPanel();
            {
                searchOptionsArea.setLayout(new BoxLayout(searchOptionsArea,
                        BoxLayout.Y_AXIS));
                JPanel containerSearchOptions = new JPanel();
                {
                    containerSearchOptions.setLayout(new BorderLayout());

                    ArrayList<String> searchOptionsProvided
                            = new ArrayList<>();
                    for (SearchOptions iterator :
                            galleryData.searchHandlers()) {
                        searchOptionsProvided.add(iterator.getName());
                    }
                    searchOptionsSelector =
                            new JComboBox<>(searchOptionsProvided.toArray());
                    JPanel sOSWrapper = new JPanel();

                    sOSWrapper.setLayout(new BorderLayout());
                    sOSWrapper.add(searchOptionsSelector, BorderLayout.CENTER);
                    sOSWrapper.setBorder(
                            BorderFactory.
                                    createEmptyBorder(
                                            2, 2,
                                            0, 2));
                    containerSearchOptions.add(sOSWrapper,
                            BorderLayout.NORTH);
                    containerSearchOptions.add(searchOptionsArea,
                            BorderLayout.CENTER);

                    searchOptionsSelector.addActionListener(actionEvent -> onSearchOptionSelect());
                    searchOptionsSelector.addItemListener(itemEvent -> onSearchOptionSelect());
                    onSearchOptionSelect();
                    searchBar.add(containerSearchOptions, BorderLayout.SOUTH);
                }

            }
        }
        add(searchBar, BorderLayout.NORTH);

        JPanel tagListSection = new JPanel();
        {
            tagListSection.setLayout(new GridLayout(2, 0));

            JPanel tagListContainer = new JPanel();
            {
                tagListContainer.setBorder(
                        BorderFactory.createTitledBorder("Search Tags"));

                JPanel topOrienter = new JPanel();
                topOrienter.setLayout(new BorderLayout());
                tagListContainer.add(topOrienter);
                searchTagsPanel = new JPanel();
                searchTagsPanel.setLayout(new BoxLayout(searchTagsPanel,
                        BoxLayout.Y_AXIS));
                topOrienter.add(searchTagsPanel, BorderLayout.NORTH);
                tagListSection.add(tagListContainer);
            }

            JPanel bottomPanel = new JPanel();
            {
                //bottomPanel.setAlignmentY(RIGHT_ALIGNMENT);
                bottomPanel.setLayout(new BorderLayout());

                secondaryTagsPanel = new JPanel();
                secondaryTagsPanel.setLayout(new BoxLayout(secondaryTagsPanel,
                        BoxLayout.Y_AXIS));
                JPanel bottomOrienter = new JPanel();
                bottomPanel.add(bottomOrienter, BorderLayout.CENTER);

                secondaryTagsPanel.setAlignmentY(RIGHT_ALIGNMENT);
                secondaryTagsPanel.setAlignmentX(RIGHT_ALIGNMENT);
                bottomOrienter.setLayout(new BorderLayout());

                bottomOrienter.add(secondaryTagsPanel, BorderLayout.SOUTH);

                if (this.isViewingEntry) {
                    bottomPanel.setBorder(
                            BorderFactory.createTitledBorder("Entry Tags"));

                    JPanel overflow = new JPanel();
                    {
                        overflow.setLayout(new BorderLayout());

                        editButton = new JButton("Tag Editor");
                        editButton.setEnabled(false);
                        overflow.add(editButton, BorderLayout.WEST);

                        addButton = new JButton("Add Tag");
                        addTagButton = addButton;
                        addButton.addActionListener(new ActionListener() {
                            @Override
                            public void actionPerformed(
                                    ActionEvent actionEvent) {
                                JPanel container = new JPanel();
                                container.setLayout(new BorderLayout());
                                container.add(tagEditor(parentTV),
                                        BorderLayout.CENTER);
                                tagAddingInProcess = container;
                                secondaryTagsPanel.add(container);
                                Toolbox.awtRepaint(secondaryTagsPanel);
                                addButton.setEnabled(false);
                            }
                        });
                        addButton.setEnabled(false);
                        overflow.add(addButton, BorderLayout.EAST);

                        bottomPanel.add(overflow, BorderLayout.SOUTH);

                    }

                } else {
                    /// showing search results
                    bottomPanel.setBorder(BorderFactory.createTitledBorder(
                            "Search Result Tags"));
                }
            }
            tagListSection.add(bottomPanel);
            add(tagListSection, BorderLayout.CENTER);
        }


    }

    public ArrayList<Entry> search(GalleryData subject) {
        updateSearchTags();
        lastSearchResultEntries = subject.search(currentSearchSettings(),
                getSearch());

        if (isViewingThumbnailSearch()) {
            if (currentSearchResultsTags == null) {
                currentSearchResultsTags = new ArrayList<>();
            }
            ArrayList<Tag> resultsTags = Tag.fromSearchResultEntries(
                    subject.entries(), currentSearchTags);
            updateResultTags();
        }
        return lastSearchResultEntries;
    }

}
