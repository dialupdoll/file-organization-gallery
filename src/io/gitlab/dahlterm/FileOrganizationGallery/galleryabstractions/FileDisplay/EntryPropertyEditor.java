package io.gitlab.dahlterm.FileOrganizationGallery.galleryabstractions.FileDisplay;

import io.gitlab.dahlterm.FileOrganizationGallery.galleryabstractions.Entry;

import javax.swing.*;

public class EntryPropertyEditor extends JPanel {
    Entry subject = null;

    public EntryPropertyEditor(Entry initSubject) {
        if (initSubject != null) {
            subject = initSubject;
        }
    }
}
