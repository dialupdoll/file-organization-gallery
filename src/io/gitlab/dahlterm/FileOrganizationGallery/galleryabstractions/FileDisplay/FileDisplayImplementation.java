package io.gitlab.dahlterm.FileOrganizationGallery.galleryabstractions.FileDisplay;

import io.gitlab.dahlterm.FileOrganizationGallery.galleryabstractions.Entry;
import io.gitlab.dahlterm.FileOrganizationGallery.other.SimpleCodeSnippet;

import javax.swing.*;
import javax.swing.filechooser.FileFilter;

public interface FileDisplayImplementation {

    boolean canDisplay(String fileExtension);

    JComponent displayFileFull(Entry target);

    JComponent displayFileThumbnail(Entry target,
                                    SimpleCodeSnippet whenClicked);

    JComponent fileDisplayExtended(Entry target);

    FileFilter supportedFilesFilter();

}
