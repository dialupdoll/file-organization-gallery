package io.gitlab.dahlterm.FileOrganizationGallery.galleryabstractions.FileDisplay.Extensions;

import io.gitlab.dahlterm.Async.Task;
import io.gitlab.dahlterm.Async.TaskQueue;
import io.gitlab.dahlterm.Caching.UsageBasedTimeInvalidatedCache;
import io.gitlab.dahlterm.FileOrganizationGallery.Constants;
import io.gitlab.dahlterm.FileOrganizationGallery.Toolbox;
import io.gitlab.dahlterm.FileOrganizationGallery.galleryabstractions.Entry;
import io.gitlab.dahlterm.FileOrganizationGallery.galleryabstractions.FileDisplay.FileDisplayImplementation;
import io.gitlab.dahlterm.FileOrganizationGallery.other.SimpleCodeSnippet;
import io.gitlab.dahlterm.Logging;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.filechooser.FileFilter;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Locale;
import java.util.Objects;
import java.util.Optional;

import static io.gitlab.dahlterm.FileOrganizationGallery.Constants.thumbnailSize;
import static io.gitlab.dahlterm.FileOrganizationGallery.Constants.waitToAvoidResourceHog;
import static io.gitlab.dahlterm.FileOrganizationGallery.Toolbox.swingThreadSafeMessage;
import static io.gitlab.dahlterm.FileOrganizationGallery.Toolbox.toBufferedImage;

public class FileDisplayImageFile implements FileDisplayImplementation {

    private static boolean canHandle(String fileExtension) {
        if (fileExtension.toLowerCase(Locale.ROOT).contains("png")) {
            return true;
        }
        if (fileExtension.toLowerCase(Locale.ROOT).contains("gif")) {
            return true;
        }
        if (fileExtension.toLowerCase(Locale.ROOT).contains("jpg")) {
            return true;
        }
        if (fileExtension.toLowerCase(Locale.ROOT).contains("jpeg")) {
            return true;
        }
        if (fileExtension.toLowerCase(Locale.ROOT).contains("bmp")) {
            return true;
        }
        if (fileExtension.toLowerCase(Locale.ROOT).contains("wbmp")) {
            return true;
        }
        return false;
    }

    @Override
    public boolean canDisplay(String fileExtension) {
        return canHandle(fileExtension);
    }

    static Rectangle scaleInto(Dimension original, Dimension target) {

        float height = original.height;
        float width = original.width;
        float paneHeight = target.height;
        float paneWidth = target.width;
        float scale = Float.min(paneWidth / width, paneHeight / height);
        int x = (int) (paneWidth / 2);
        x -= width * scale / 2;
        int y = (int) (paneHeight / 2);
        y -= height * scale / 2;
        return new Rectangle(x, y, (int) (width * scale), (int) (height * scale));
    }

    @Override
    public JComponent displayFileFull(Entry target) {
        JPanel result = new JPanel();
        result.setLayout(new BorderLayout());

        final JProgressBar loadingAnimation = new JProgressBar();
        loadingAnimation.setIndeterminate(true);
        loadingAnimation.setString("Loading image");
        JPanel holder = new JPanel();
        holder.add(loadingAnimation);
        JPanel renderer = new JPanel() {
            final JComponent comp = this;
            final SimpleCodeSnippet notifier = () ->
            {
                comp.remove(holder);
                Toolbox.awtRepaint(comp);
            };
            Image lastScaled = null;
            BufferedImage myPicture = CachingImageHandler.loadOrFetch(target, false,
                    notifier);
            boolean hasWaited = true;

            public void paint(Graphics graphics) {

                super.paint(graphics);
                if (myPicture == null) {
                    myPicture = CachingImageHandler.loadOrFetch(target, false,
                            notifier);
                }

                if (myPicture == null) {
                    hasWaited = true;
                    /// this means we're waiting for the notifier callback
                    return;
                }
                /*RenderingHints rh = new RenderingHints(
                        RenderingHints.KEY_INTERPOLATION,
                        RenderingHints.VALUE_INTERPOLATION_BICUBIC);
                g.setRenderingHints(rh);*/
                if (hasWaited) {
                    int hints = Image.SCALE_AREA_AVERAGING;
                    Rectangle resized = scaleInto(new Dimension(myPicture.getWidth(),
                                    myPicture.getHeight()),
                            new Dimension(this.getWidth() - 5,
                                    this.getHeight() - 5));
                    lastScaled = myPicture.getScaledInstance(
                            resized.width, resized.height,
                            hints
                    );
                    hasWaited = false;
                } else {
                    new Thread(() -> {
                        Runnable safety = () -> {
                            try {
                                Thread.sleep(waitToAvoidResourceHog());
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                            hasWaited = true;
                            notifier.update();
                        };
                        SwingUtilities.invokeLater(safety);
                    }).start();
                }

                Graphics2D advancedGraphics = (Graphics2D) graphics;

                advancedGraphics.drawImage(lastScaled,
                        getWidth() / 2 - lastScaled.getWidth(null) / 2,
                        getHeight() / 2 - lastScaled.getHeight(null) / 2,
                        lastScaled.getWidth(null),
                        lastScaled.getHeight(null),
                        null);
            }
        };

        result.add(renderer, BorderLayout.CENTER);
        renderer.setLayout(new BorderLayout());
        renderer.add(holder, BorderLayout.CENTER);
        return result;
    }

    @Override
    public JComponent displayFileThumbnail(Entry target,
                                           SimpleCodeSnippet whenClicked) {
        JPanel container = new JPanel();
        container.setLayout(new BorderLayout());
        container.setPreferredSize(new Dimension(thumbnailSize(), thumbnailSize()));
        container.setMinimumSize(new Dimension(thumbnailSize(), thumbnailSize()));
        container.setMaximumSize(new Dimension(thumbnailSize(), thumbnailSize()));
        final JButton result = new JButton("Loading") {
            final Component me = this;
            final SimpleCodeSnippet updater = () ->
            {
                Toolbox.awtRepaint(me);
            };
            BufferedImage myPicture = null;

            @Override
            public void paint(Graphics g) {
                if (myPicture == null) {
                    myPicture = CachingImageHandler.loadOrFetch(target, true,
                            updater);
                }
                if (myPicture == null) {
                    super.paint(g);
                    return;
                }
                ImageIcon icon = new ImageIcon(myPicture);
                setIcon(icon);
                setText("");
                super.paint(g);
            }
        };

        result.addActionListener(actionEvent -> swingThreadSafeMessage(whenClicked));
        container.add(result, BorderLayout.CENTER);
        return container;
    }

    @Override
    public JComponent fileDisplayExtended(Entry target) {
        JPanel result = new JPanel();
        {
            result.setLayout(new FlowLayout());
            JButton clipboardCopy = new JButton("Copy image to clipboard");
            clipboardCopy.addActionListener(new ActionListener() {

                @Override
                public void actionPerformed(ActionEvent e) {
                    File file = target.wrappedEntry();
                    try {
                        Toolbox.writeImageToClipboard(ImageIO.read(file));
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            });
            result.add(clipboardCopy);
        }
        return result;
    }

    @Override
    public FileFilter supportedFilesFilter() {
        return new FileFilter() {
            @Override
            public boolean accept(File file) {
                Optional<String> extensionResult =
                        Toolbox.getExtensionByStringHandling(file.getPath());
                if (extensionResult.isPresent()) {
                    String fileExtension = extensionResult.get();
                    if (canHandle(fileExtension)) {
                        return true;
                    }
                }
                return false;
            }

            @Override
            public String getDescription() {
                return "ImageIO supported image files (png, jpg, bmp, gif)";
            }
        };
    }


    public static class CachingImageHandler {
        private static class CachedImageInfo {
            BufferedImage image = null;
            int maxThumbnailSize = -1;
            int failCount = 5;
            Entry source = null;
            SimpleCodeSnippet updater = null;
            int keepSeconds = -1;

            @Override
            public boolean equals(Object o) {
                if (this == o)
                    return true;
                if (o == null || getClass() != o.getClass())
                    return false;
                CachedImageInfo that = (CachedImageInfo) o;
                return maxThumbnailSize == that.maxThumbnailSize && failCount == that.failCount && keepSeconds == that.keepSeconds && Objects.equals(image, that.image) && Objects.equals(source, that.source) && Objects.equals(updater, that.updater);
            }

            @Override
            public int hashCode() {
                return Objects.hash(image, maxThumbnailSize, failCount, source, updater, keepSeconds);
            }
        }

        static UsageBasedTimeInvalidatedCache<File, CachedImageInfo> thumbSizedCache =
                new UsageBasedTimeInvalidatedCache<>(5);
        static UsageBasedTimeInvalidatedCache<File, CachedImageInfo> fullSizedCache =
                new UsageBasedTimeInvalidatedCache<>(5);

        public static TaskQueue fullImageQueue = new TaskQueue();
        public static TaskQueue slowerTaskQueue = new TaskQueue();

        private static BufferedImage loadOrFetch(Entry target,
                                                 boolean requireThumbnail,
                                                 SimpleCodeSnippet updater) {

            Logging.log(Logging.FLOW, "Load Or Fetch began.");
            File focus = target.wrappedEntry();

            UsageBasedTimeInvalidatedCache<File, CachedImageInfo> cacheToCheck;
            if (requireThumbnail) {
                cacheToCheck = thumbSizedCache;
            } else {
                cacheToCheck = fullSizedCache;
            }
            CachedImageInfo result = cacheToCheck.fetchOrNull(focus);

            if (result != null && result.image != null) {
                Logging.log(Logging.FLOW, "Found image, returning");
                /// already set the cache to check to whichever one is right.
                // this is all we need to do, if it's a thumbnail we have it
                // now.
                if (updater != null) {
                    Toolbox.swingThreadSafeMessage(updater);
                }
                return result.image;
            } else {
                /// pull from fullsized cache if present, and resize instead of
                // reloading the image entirely

                CachedImageInfo request = new CachedImageInfo();
                request.source = target;
                request.updater = updater;


                if (requireThumbnail) {
                    cacheToCheck = fullSizedCache;
                    result = cacheToCheck.fetchOrNull(focus);

                    request.keepSeconds = Constants.secondsToKeep.thumbnail();
                    request.maxThumbnailSize = thumbnailSize() - 5;

                    if (result == null) {
                        /// just do a full request for thumbbnail, not just a
                        // request for resize
                        LoadImageTask lit = new LoadImageTask();
                        lit.imageInfo = request;
                        Logging.log(Logging.FLOW, "adding full thumbnail " +
                                "request.");
                        slowerTaskQueue.add(lit);
                        slowerTaskQueue.startAsyncIfNotWorking();
                    } else {
                        /// request resize task
                        ResizeImageTask rit = new ResizeImageTask();
                        /// build the image info
                        {
                            CachedImageInfo info = new CachedImageInfo();
                            info.image = result.image;
                            info.source = target;
                            info.maxThumbnailSize = thumbnailSize();
                            info.updater = updater;
                            info.keepSeconds = Constants.secondsToKeep.thumbnail();
                            rit.imageInfo = info;
                        }
                        Logging.log(Logging.FLOW, "adding resize thumbnail " +
                                "request.");
                        slowerTaskQueue.add(rit);
                        slowerTaskQueue.startAsyncIfNotWorking();
                    }
                } else {
                    request.maxThumbnailSize = -1;
                    request.keepSeconds = Constants.secondsToKeep.full();
                    LoadImageTask lit = new LoadImageTask();
                    lit.imageInfo = request;
                    Logging.log(Logging.FLOW, "adding load " +
                            "request.");
                    fullImageQueue.add(lit);
                    fullImageQueue.startAsyncIfNotWorking();
                }

            }
            return null;
        }

        private static class LoadImageTask extends Task.SimpleTask {
            CachedImageInfo imageInfo = null;
            ResizeImageTask resize = null;

            @Override
            public TaskStatus completeTask() {
                if (imageInfo.maxThumbnailSize != -1) {
                    if (!fullImageQueue.queueEmpty()) {
                        return TaskStatus.WAITING;
                    }
                }
                Logging.log(Logging.FLOW,
                        "attempting Load Image Task");
                try {
                    synchronized (fullSizedCache) {
                        if (fullSizedCache.checkIfCached(imageInfo.source.wrappedEntry())) {
                            Toolbox.swingThreadSafeMessage(imageInfo.updater);
                            return TaskStatus.SUCCESSFUL;
                        }
                    }
                    if (resize != null) {
                        return resize.completeTask();
                    }
                    if (imageInfo.maxThumbnailSize == -1) {
                        synchronized (fullSizedCache) {
                            if (fullSizedCache.checkIfCached(imageInfo.source.wrappedEntry())) {
                                /// it's already there, update the hold time
                                fullSizedCache.fetchOrNull(imageInfo.source.wrappedEntry());
                            } else {
                                BufferedImage result =
                                        ImageIO.read(imageInfo.source.wrappedEntry());
                                imageInfo.image = result;
                                fullSizedCache.add(imageInfo.source.wrappedEntry(),
                                        imageInfo, imageInfo.keepSeconds);
                            }
                        }

                        Toolbox.swingThreadSafeMessage(imageInfo.updater);

                        slowerTaskQueue.removeMatching(this);
                        fullImageQueue.removeMatching(this);
                        return TaskStatus.SUCCESSFUL;
                    } else {

                        BufferedImage result =
                                ImageIO.read(imageInfo.source.wrappedEntry());
                        imageInfo.image = result;
                        resize = new ResizeImageTask();
                        resize.imageInfo = imageInfo;
                        return resize.completeTask();
                    }
                } catch (IOException e) {
                    Logging.log(Logging.INFORMATION,
                            "Couldn't load image " +
                                    imageInfo.source.wrappedEntry().getName()
                                    + System.lineSeparator()
                                    + e.getMessage());
                    imageInfo.failCount--;
                    if (imageInfo.failCount < 0) {
                        return TaskStatus.FAILED_NO_RETRY;
                    } else {
                        return TaskStatus.FAILED_AWAITING_RETRY;
                    }
                }
            }

            @Override
            public boolean equalTo(Task t) {
                if (t instanceof LoadImageTask) {
                    LoadImageTask lit = (LoadImageTask) t;
                    if (lit.imageInfo.source.equalTo(imageInfo.source))
                        return true;
                }
                return false;
            }
        }

        private static class ResizeImageTask extends Task.SimpleTask {
            CachedImageInfo imageInfo = null;

            @Override
            public TaskStatus completeTask() {
                if (fullImageQueue.queueEmpty()) {
                    Logging.log(Logging.FLOW,
                            "attempting Resize Image Task");

                    try {
                        if (imageInfo.image != null) {
                            Rectangle resized = scaleInto(new Dimension(
                                            imageInfo.image.getWidth(),
                                            imageInfo.image.getHeight()),
                                    new Dimension(imageInfo.maxThumbnailSize - 6,
                                            imageInfo.maxThumbnailSize - 6));
                            int hints = Image.SCALE_SMOOTH;
                            //hints = Image.SCALE_FAST;
                            imageInfo.image = toBufferedImage(
                                    imageInfo.image.getScaledInstance(
                                            resized.width, resized.height,
                                            hints
                                    ));
                            synchronized (thumbSizedCache) {
                                if (thumbSizedCache.checkIfCached(imageInfo.source.wrappedEntry())) {
                                    thumbSizedCache.fetchOrNull(imageInfo.source.wrappedEntry());
                                } else {
                                    thumbSizedCache.add(imageInfo.source.wrappedEntry(),
                                            imageInfo, Constants.secondsToKeep.thumbnail());
                                }
                            }
                            slowerTaskQueue.removeMatching(this);
                            fullImageQueue.removeMatching(this);
                            Toolbox.swingThreadSafeMessage(imageInfo.updater);
                            return TaskStatus.SUCCESSFUL;
                        } else {
                            Logging.log(Logging.ERROR, "Where's the image?");
                            return TaskStatus.FAILED_AWAITING_RETRY;
                        }
                    } catch (Exception e) {
                        Logging.log(Logging.INFORMATION,
                                "Couldn't load image " +
                                        imageInfo.source.wrappedEntry().getName()
                                        + System.lineSeparator()
                                        + e.getMessage());
                        imageInfo.failCount--;
                        if (imageInfo.failCount < 0) {
                            return TaskStatus.FAILED_NO_RETRY;
                        } else {
                            return TaskStatus.FAILED_AWAITING_RETRY;
                        }
                    }
                } else {
                    Thread.yield();
                    Thread.onSpinWait();
                    return TaskStatus.WAITING;
                }
            }

            @Override
            public boolean equalTo(Task t) {
                if (t instanceof ResizeImageTask) {
                    ResizeImageTask lit = (ResizeImageTask) t;
                    if (lit.imageInfo.source.equalTo(imageInfo.source))
                        return true;
                }
                return false;
            }
        }

    }

}
