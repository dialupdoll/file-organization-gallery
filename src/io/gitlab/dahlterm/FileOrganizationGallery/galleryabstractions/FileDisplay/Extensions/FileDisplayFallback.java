package io.gitlab.dahlterm.FileOrganizationGallery.galleryabstractions.FileDisplay.Extensions;

import io.gitlab.dahlterm.FileOrganizationGallery.Toolbox;
import io.gitlab.dahlterm.FileOrganizationGallery.galleryabstractions.Entry;
import io.gitlab.dahlterm.FileOrganizationGallery.galleryabstractions.FileDisplay.FileDisplayImplementation;
import io.gitlab.dahlterm.FileOrganizationGallery.other.SimpleCodeSnippet;

import javax.swing.*;
import javax.swing.filechooser.FileFilter;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class FileDisplayFallback implements FileDisplayImplementation {

    @Override
    public boolean canDisplay(String fileExtension) {
        return true;
    }

    @Override
    public JPanel displayFileThumbnail(Entry entry,
                                       SimpleCodeSnippet whenClicked) {
        File target = entry.wrappedEntry();
        JPanel result = new JPanel();
        result.setLayout(new BorderLayout());
        String displayText =
                "<html><body><p><center>" +
                        Toolbox.getExtensionByStringHandling(
                                target.getName()).get()
                        + "</center> <br> " + target.getName().
                        substring(0, 5) + " …" +
                        "</p></body></html>";
        JButton clicker = new JButton(displayText);

        //result.setMaximumSize(new Dimension(150, 150));
        //result.setPreferredSize(new Dimension(150, 150));
        //result.setMinimumSize(new Dimension(150, 150));

        clicker.setMaximumSize(new Dimension(150, 150));
        clicker.setPreferredSize(new Dimension(150, 150));
        clicker.setMinimumSize(new Dimension(150, 150));

        clicker.addActionListener(actionEvent -> whenClicked.update());
        result.add(clicker, BorderLayout.CENTER);
        return result;
    }

    @Override
    public JPanel displayFileFull(Entry entry) {
        File target = entry.wrappedEntry();
        JPanel result = new JPanel();
        {
            result.setLayout(new BorderLayout());
            String type;
            if (target.isFile())
                type = "File: ";
            else if (target.isDirectory())
                type = "Directory: ";
            else
                type = "Unknown Object: ";
            JTextArea information = new JTextArea(
                    type + target.getAbsolutePath() + System.lineSeparator() +
                            "Size: " + target.length() + System.lineSeparator()
            );
            information.setLineWrap(true);
            information.setWrapStyleWord(true);
            information.setEditable(false);
            information.setAlignmentX(0.5f);
            information.setAlignmentY(0.5f);
            information.setMaximumSize(new Dimension(Integer.MAX_VALUE, Integer.MAX_VALUE));
            result.add(information, BorderLayout.NORTH);
        }
        return result;
    }

    @Override
    public JPanel fileDisplayExtended(Entry entry) {
        JPanel result = new JPanel();
        {
            int totalSupported = 0;
            result.setLayout(new BoxLayout(result, BoxLayout.Y_AXIS));

            ArrayList<JComponent> unsupported = new ArrayList<>();
            if (Desktop.isDesktopSupported()) {
                Desktop desk = Desktop.getDesktop();

                if (desk.isSupported(Desktop.Action.OPEN)) {
                    JPanel openFile = new JPanel();
                    {
                        openFile.setLayout(new BorderLayout());
                        JButton openWithOS = new JButton("Open");
                        JTextArea extendedInformation = new JTextArea(
                                "Opens with default application according to operating system.");
                        extendedInformation.setLineWrap(true);
                        extendedInformation.setWrapStyleWord(true);
                        extendedInformation.setEditable(false);
                        extendedInformation.setMaximumSize(new Dimension(Integer.MAX_VALUE, Integer.MAX_VALUE));
                        openFile.add(extendedInformation, BorderLayout.CENTER);
                        openFile.add(openWithOS, BorderLayout.EAST);

                        openWithOS.addActionListener(actionEvent ->
                        {
                            try {
                                desk.open(entry.wrappedEntry());
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        });
                        result.add(openFile);
                    }

                    totalSupported++;
                } else {
                    JTextArea extendedInformation = new JTextArea("Default application integration unavailable");
                    extendedInformation.setLineWrap(true);
                    extendedInformation.setWrapStyleWord(true);
                    extendedInformation.setEditable(false);
                    extendedInformation.setMaximumSize(new Dimension(Integer.MAX_VALUE, Integer.MAX_VALUE));
                    unsupported.add(extendedInformation);
                }

                if (desk.isSupported(Desktop.Action.EDIT)) {
                    JPanel editFile = new JPanel();
                    {
                        editFile.setLayout(new BorderLayout());
                        JButton openWithOS = new JButton("Edit");
                        JTextArea extendedInformation = new JTextArea(
                                "Opens with default editor according to operating system.");
                        extendedInformation.setLineWrap(true);
                        extendedInformation.setWrapStyleWord(true);
                        extendedInformation.setEditable(false);
                        extendedInformation.setMaximumSize(new Dimension(Integer.MAX_VALUE, Integer.MAX_VALUE));
                        editFile.add(extendedInformation, BorderLayout.CENTER);
                        editFile.add(openWithOS, BorderLayout.EAST);

                        openWithOS.addActionListener(actionEvent ->
                        {
                            try {
                                desk.edit(entry.wrappedEntry());
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        });
                        result.add(editFile);
                    }
                    totalSupported++;
                } else {
                    JTextArea extendedInformation = new JTextArea("Default editor integration unavailable");
                    extendedInformation.setLineWrap(true);
                    extendedInformation.setWrapStyleWord(true);
                    extendedInformation.setEditable(false);
                    extendedInformation.setMaximumSize(new Dimension(Integer.MAX_VALUE, Integer.MAX_VALUE));
                    unsupported.add(extendedInformation);
                }

                if (desk.isSupported(Desktop.Action.BROWSE)) {
                    JPanel browseFile = new JPanel();
                    {
                        browseFile.setLayout(new BorderLayout());
                        JButton openWithOS = new JButton("Browse");
                        JTextArea extendedInformation = new JTextArea(
                                "Opens with default web browser according to operating system.");
                        extendedInformation.setLineWrap(true);
                        extendedInformation.setWrapStyleWord(true);
                        extendedInformation.setEditable(false);
                        extendedInformation.setMaximumSize(new Dimension(Integer.MAX_VALUE, Integer.MAX_VALUE));
                        browseFile.add(extendedInformation, BorderLayout.CENTER);
                        browseFile.add(openWithOS, BorderLayout.EAST);

                        openWithOS.addActionListener(actionEvent ->
                        {
                            try {
                                desk.browse(entry.wrappedEntry().toURI());
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        });
                        result.add(browseFile);
                    }
                    totalSupported++;
                } else {
                    JTextArea extendedInformation = new JTextArea("Default web browser integration unavailable");
                    extendedInformation.setLineWrap(true);
                    extendedInformation.setWrapStyleWord(true);
                    extendedInformation.setEditable(false);
                    extendedInformation.setMaximumSize(new Dimension(Integer.MAX_VALUE, Integer.MAX_VALUE));
                    unsupported.add(extendedInformation);
                }

                if (desk.isSupported(Desktop.Action.BROWSE_FILE_DIR)) {
                    JPanel browseFileDir = new JPanel();
                    {
                        browseFileDir.setLayout(new BorderLayout());
                        JButton openWithOS = new JButton("Show in Default File Browser");
                        JTextArea extendedInformation = new JTextArea(
                                "Opens with default file browser according to operating system.");
                        extendedInformation.setLineWrap(true);
                        extendedInformation.setWrapStyleWord(true);
                        extendedInformation.setEditable(false);
                        extendedInformation.setMaximumSize(new Dimension(Integer.MAX_VALUE, Integer.MAX_VALUE));
                        browseFileDir.add(extendedInformation, BorderLayout.CENTER);
                        browseFileDir.add(openWithOS, BorderLayout.EAST);

                        openWithOS.addActionListener(actionEvent ->
                        {
                            try {
                                desk.browseFileDirectory(entry.wrappedEntry());
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        });
                        result.add(browseFileDir);
                    }
                    totalSupported++;
                } else {
                    JTextArea extendedInformation = new JTextArea("File browser integration unavailable");
                    extendedInformation.setLineWrap(true);
                    extendedInformation.setWrapStyleWord(true);
                    extendedInformation.setEditable(false);
                    extendedInformation.setMaximumSize(new Dimension(Integer.MAX_VALUE, Integer.MAX_VALUE));
                    unsupported.add(extendedInformation);
                }
            }
            if (!unsupported.isEmpty()) {
                JPanel border = new JPanel();
                border.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10),
                        BorderFactory.createLineBorder(Color.black)));
                result.add(border);
                for (JComponent c : unsupported) {
                    result.add(c);
                }
            }
            if (totalSupported == 0) {
                result.add(new JLabel("Not able to support extended actions for this file!"));
            }
        }
        return result;
    }

    @Override
    public FileFilter supportedFilesFilter() {
        return null;
    }
}
