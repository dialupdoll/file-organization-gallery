package io.gitlab.dahlterm.FileOrganizationGallery;

import io.gitlab.dahlterm.FileOrganizationGallery.other.SimpleCodeSnippet;

import javax.swing.*;
import java.awt.*;
import java.awt.datatransfer.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Optional;

public class Toolbox {
    private static class CopyImagetoClipBoard implements ClipboardOwner {
        private class TransferableImage implements Transferable {

            Image i;

            public TransferableImage(Image i) {
                this.i = i;
            }

            public Object getTransferData(DataFlavor flavor)
                    throws UnsupportedFlavorException, IOException {
                if (flavor.equals(DataFlavor.imageFlavor) && i != null) {
                    return i;
                } else {
                    throw new UnsupportedFlavorException(flavor);
                }
            }

            public DataFlavor[] getTransferDataFlavors() {
                DataFlavor[] flavors = new DataFlavor[1];
                flavors[0] = DataFlavor.imageFlavor;
                return flavors;
            }

            public boolean isDataFlavorSupported(DataFlavor flavor) {
                DataFlavor[] flavors = getTransferDataFlavors();
                for (int i = 0; i < flavors.length; i++) {
                    if (flavor.equals(flavors[i])) {
                        return true;
                    }
                }

                return false;
            }
        }

        public void copyImage(BufferedImage bi) {
            TransferableImage trans = new TransferableImage(bi);
            Clipboard c = Toolkit.getDefaultToolkit().getSystemClipboard();
            c.setContents(trans, this);
        }

        @Override
        public void lostOwnership(Clipboard clipboard, Transferable transferable) {

        }
    }

    public static ClipboardOwner writeImageToClipboard(BufferedImage image) {
        CopyImagetoClipBoard ci = new CopyImagetoClipBoard();
        ci.copyImage(image);
        return ci;
    }

    public static void writeStringToClipboard(String s, ClipboardOwner owner) {
        Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
        Transferable transferable = new StringSelection(s);
        clipboard.setContents(transferable, owner);
    }

    public static void swingThreadSafeMessage(SimpleCodeSnippet target) {
        new Thread(() -> {
            Runnable safety = target::update;
            SwingUtilities.invokeLater(safety);
        }).start();

    }

    public static Optional<String>
    getExtensionByStringHandling(String filename) {
        return Optional.ofNullable(filename)
                .filter(f -> f.contains("."))
                .map(f -> f.
                        substring(filename.lastIndexOf(".") + 1));
    }

    public static BufferedImage toBufferedImage(Image img) {
        if (img instanceof BufferedImage) {
            return (BufferedImage) img;
        }

        // Create a buffered image with transparency
        BufferedImage bimage = new BufferedImage(
                img.getWidth(null),
                img.getHeight(null),
                BufferedImage.TYPE_INT_ARGB);

        // Draw the image on to the buffered image
        Graphics2D bGr = bimage.createGraphics();
        bGr.drawImage(img, 0, 0, null);
        bGr.dispose();

        // Return the buffered image
        return bimage;
    }

    public static void awtRepaint(Component c) {
        if (c == null) {
            System.out.println("Error, trying to repaint null");
            return;
        }
        swingThreadSafeMessage(() -> {
            c.repaint();
            c.revalidate();
            c.invalidate();
        });
    }
}
