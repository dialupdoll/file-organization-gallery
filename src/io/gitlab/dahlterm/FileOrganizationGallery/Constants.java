package io.gitlab.dahlterm.FileOrganizationGallery;

public class Constants {

    public static int thumbnailSize() {
        return 150;
    }

    public static int waitToAvoidResourceHog() {
        return 100;
    }

    public static class secondsToKeep {
        public static int thumbnail() {
            return 15;
        }

        public static int full() {
            return 5;
        }
    }
}
