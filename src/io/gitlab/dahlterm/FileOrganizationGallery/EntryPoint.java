package io.gitlab.dahlterm.FileOrganizationGallery;

import io.gitlab.dahlterm.FileOrganizationGallery.ui.GalleryListWindow;
import io.gitlab.dahlterm.Logging;

import javax.swing.*;

public class EntryPoint {

    ////

    public static int windowCount = 0;

    public static void checkShouldClose() {
        windowCount--;
        Thread delayJustInCaseAWindowOpens = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(1000);
                    if (windowCount == 0) {
                        System.exit(0);
                    } else {
                        /// a window was closed
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            }
        });
        delayJustInCaseAWindowOpens.run();
    }

    public static void main(String arguments[]) {
        Logging.shouldLogTypeSet(Logging.FLOW, false)
                .shouldLogTypeSet(Logging.ERROR, true)
                .shouldLogTypeSet(Logging.INFORMATION, true);
        Exception e = null;
        try {
            // Set L&F
            //UIManager.setLookAndFeel("com.sun.java.swing.plaf.motif.MotifLookAndFeel");
            UIManager.setLookAndFeel("com.sun.java.swing.plaf.gtk.GTKLookAndFeel");
        } catch (Exception ex) {
            // handle exception
            e = ex;
            try {
                UIManager.setLookAndFeel("com.sun.java.swing.plaf.motif.MotifLookAndFeel");
                e = null;
            } catch (Exception e2) {
                if (e != null)
                    e2.printStackTrace();
            }
        } finally {
            if (e != null) {
                e.printStackTrace();
            }
        }
        new GalleryListWindow();
    }
}
