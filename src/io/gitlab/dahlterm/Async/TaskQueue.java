package io.gitlab.dahlterm.Async;

import io.gitlab.dahlterm.FileOrganizationGallery.other.SimpleCodeSnippet;
import io.gitlab.dahlterm.Logging;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

public class TaskQueue {
    private AtomicInteger totalTasks = new AtomicInteger(0);
    private AtomicInteger completedTasks = new AtomicInteger(0);
    private final LinkedList<Task> waiting = new LinkedList<>();
    private final ArrayList<SimpleCodeSnippet> stateChangeListeners =
            new ArrayList<>();
    private AtomicBoolean running = new AtomicBoolean(false);
    /// below makes it more readable when passing final reference to owner of
    // thread running to work on the tasks.
    private final TaskQueue self = this;

    public void add(Task subject) {
        synchronized (waiting) {
            int moveTarget = -1;

            for (Task t : waiting) {
                synchronized (t) {
                    if (t.equalTo(subject)) {
                        moveTarget = waiting.indexOf(t);
                        if (moveTarget != -1) {
                            Task sub = waiting.get(moveTarget);
                            waiting.remove(moveTarget);
                            removeMatching(subject);
                            waiting.add(0, sub);
                            // move to the front of the line
                            return;
                        }
                    }
                }
            }
            waiting.add(subject);
            totalTasks.incrementAndGet();
        }
    }

    public void removeMatching(Task match) {
        synchronized (waiting) {
            ArrayList<Task> matchesToRemove = new ArrayList<>();
            for (Task t : waiting) {
                if (t.equalTo(match)) {
                    matchesToRemove.add(t);
                }
            }
            for (Task t : matchesToRemove) {
                waiting.remove(t);
            }
        }
    }

    public boolean queueEmpty() {
        synchronized (waiting) {
            return waiting.size() == 0;
        }
    }

    public boolean isWorking() {
        return running.get();
    }

    public void resetCounters() {
        totalTasks.set(0);
        completedTasks.set(0);
    }

    private void updateListeners() {
        if (!stateChangeListeners.isEmpty()) {
            for (SimpleCodeSnippet listener : stateChangeListeners) {
                listener.update();
            }
        }
    }

    public int waitingTasks() {
        return waiting.size();
    }

    public int maxTasks() {
        return totalTasks.get();
    }

    public int completedTasks() {
        return completedTasks.get();
    }

    public void startInLineIfNotWorking() {
        if (running.compareAndSet(false, true)) {
            Logging.log(Logging.FLOW, "Starting task queue inline.");
            runnable().run();
        }
    }

    public void startAsyncIfNotWorking() {
        if (running.compareAndSet(false, true)) {
            Logging.log(Logging.FLOW, "Starting task queue async.");
            new Thread(runnable()).start();
        }
    }

    public void addStateChangeListener(SimpleCodeSnippet event) {
        synchronized (stateChangeListeners) {
            if (!stateChangeListeners.contains(event)) {
                stateChangeListeners.add(event);
            }
        }
    }

    public void removeStateChangeListener(SimpleCodeSnippet event) {
        synchronized (stateChangeListeners) {
            stateChangeListeners.remove(event);
        }
    }

    public boolean containsStateChangeListener(SimpleCodeSnippet event) {
        synchronized (stateChangeListeners) {
            if (stateChangeListeners.contains(event)) {
                return true;
            }
        }
        return false;
    }

    private Runnable runnable() {
        return new Runnable() {
            /// when passing into an anonymous class, we need a final reference
            private final TaskQueue parent = self;

            @Override
            public void run() {
                Logging.log(Logging.FLOW, "Task queue entering loop.");
                while (!queueEmpty()) {
                    Thread.yield();

                    Logging.log(Logging.FLOW, "Task queue running.");
                    Task current = null;
                    synchronized (waiting) {
                        current = waiting.pop();
                    }
                    if (current != null) {
                        Task.TaskStatus status = current.completeTask();
                        if (current instanceof Task.SimpleTask) {
                            ((Task.SimpleTask) current).status = status;
                        }
                        switch (status) {
                            case FAILED_AWAITING_RETRY:
                                synchronized (waiting) {
                                    waiting.add(current);
                                }
                                Logging.log(Logging.FLOW, "Task failed, " +
                                        "retrying.");
                                updateListeners();
                            case FAILED_NO_RETRY:
                                Logging.log(Logging.FLOW, "Task failed, not " +
                                        "retrying.");
                                completedTasks.incrementAndGet();
                                updateListeners();
                                break;
                            case WAITING:
                                Logging.log(Logging.FLOW, "Waiting returned, " +
                                        "will run later from front of queue");
                                waiting.add(0, current);

                                updateListeners();
                                break;
                            case SUCCESSFUL:
                                Logging.log(Logging.FLOW, "Task successful.");
                                completedTasks.incrementAndGet();
                                updateListeners();
                                break;
                        }
                    }
                    updateListeners();
                }
                Logging.log(Logging.FLOW, "Task queue exiting loop.");
                /// FINALLY, STOP THE TASK FROM BEING SHOWN AS RUNNING
                running.set(false);
                updateListeners();
            }
        };
    }
}
