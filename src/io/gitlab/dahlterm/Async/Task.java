package io.gitlab.dahlterm.Async;

public interface Task {

    TaskStatus completeTask();

    TaskStatus status();

    String stringStatus();

    int completionCount();

    int totalCompletionNecessary();

    boolean equalTo(Task t);

    public static abstract class SimpleTask implements Task {
        TaskStatus status = TaskStatus.WAITING;
        private final String noErrorGiven = "No error reported by SimpleTask. ";
        String errorGiven = noErrorGiven;

        @Override
        public int completionCount() {
            return -1;
        }

        @Override
        public int totalCompletionNecessary() {
            return -1;
        }

        public TaskStatus status() {
            return status;
        }

        public String stringStatus() {
            switch (status()) {
                case WAITING:
                    return "Waiting to be run.";
                case SUCCESSFUL:
                    return "No error, ran successfully.";
                case FAILED_NO_RETRY:
                    if (errorGiven == noErrorGiven) {
                        return "Failed, not awaiting retry. " + noErrorGiven + this.toString();
                    } else {
                        return "Failed, not awaiting retry. " + errorGiven;
                    }
                case FAILED_AWAITING_RETRY:
                    if (errorGiven == noErrorGiven) {
                        return "Failed, awaiting retry. " + noErrorGiven + this.toString();
                    } else {
                        return "Failed, awaiting retry. " + errorGiven;
                    }
            }
            return "Internal error in SimpleTask";
        }


    }

    public static enum TaskStatus {
        WAITING, SUCCESSFUL, FAILED_NO_RETRY, FAILED_AWAITING_RETRY
    }
}
