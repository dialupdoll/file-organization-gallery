package io.gitlab.dahlterm.Async;

import io.gitlab.dahlterm.Logging;

public class SubTaskQueueTask extends TaskQueue implements Task {

    @Override
    public TaskStatus completeTask() {
        try {
            startInLineIfNotWorking();
        } catch (Exception e) {
            e.printStackTrace();
            return TaskStatus.FAILED_NO_RETRY;
        }
        return TaskStatus.SUCCESSFUL;
    }

    @Override
    public void startAsyncIfNotWorking() {
        Logging.log(Logging.ERROR,
                "Cannot start sub task queue task as async - goal is"
                        + " to run it synchronously in main task queue thread"
                        + ". instead, starting inline.");
        startInLineIfNotWorking();
    }

    @Override
    public TaskStatus status() {
        if (super.queueEmpty()) {
            return TaskStatus.SUCCESSFUL;
        }
        if (!super.isWorking()) {
            return TaskStatus.WAITING;
        }
        // TODO: Might need more thought
        return null;
    }

    @Override
    public String stringStatus() {
        return null;
    }

    @Override
    public int completionCount() {
        return -1;
    }

    @Override
    public int totalCompletionNecessary() {
        return -1;
    }

    @Override
    public boolean equalTo(Task t) {
        if (t instanceof SubTaskQueueTask) {
            return false;
            // TODO: needs work - how do we discern if a
            // TODO: stqt is equivalent without unusual amounts of work?
        } else {
            return false;
        }
    }
}
