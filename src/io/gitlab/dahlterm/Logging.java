package io.gitlab.dahlterm;

import java.util.BitSet;

public class Logging {
    public static final int ERROR = 0;
    public static final int INFORMATION = 1;
    public static final int FLOW = 2;

    public static interface LogHandler {
        LogHandler shouldLogTypeSet(int indexOfLogType, boolean value);

        boolean shouldLogTypeGet(int indexOfLogType);

        LogHandler log(int indexOfLogType, String message);
    }

    private static class SystemOutLogHandler implements LogHandler {
        BitSet shouldLog = new BitSet();

        @Override
        public LogHandler shouldLogTypeSet(int indexOfLogType, boolean value) {
            if (value) {
                shouldLog.set(indexOfLogType);
            } else {
                shouldLog.clear(indexOfLogType);
            }
            return this;
        }

        @Override
        public boolean shouldLogTypeGet(int indexOfLogType) {
            return shouldLog.get(indexOfLogType);
        }

        @Override
        public LogHandler log(int indexOfLogType, String message) {
            if (shouldLogTypeGet(indexOfLogType)) {
                System.out.println(message);
            }
            return this;
        }
    }

    private static LogHandler internal = new SystemOutLogHandler();

    public static LogHandler shouldLogTypeSet(int indexOfLogType,
                                              boolean value) {
        return internal.shouldLogTypeSet(indexOfLogType, value);
    }

    public static boolean shouldLogTypeGet(int indexOfLogType) {
        return internal.shouldLogTypeGet(indexOfLogType);
    }

    public static LogHandler log(int indexOfLogType, String message) {
        return internal.log(indexOfLogType, message);
    }
}
